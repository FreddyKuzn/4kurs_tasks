#include "stdafx.h"
#include "GDIClass.h"
using namespace Gdiplus;
IMPLEMENT_DYNAMIC(GDIClass, CStatic)
ULONG_PTR gdiPlusToken;
GDIClass::GDIClass()
{
	GdiplusStartupInput gdiPlusStartapInput;
	if (GdiplusStartup(&gdiPlusToken, &gdiPlusStartapInput, NULL) != Ok)
	{
		MessageBox(L"Init error", L"Error Message", MB_OK);
		exit(0);
	}
}
GDIClass::~GDIClass()
{
	GdiplusShutdown(gdiPlusToken);
}

BEGIN_MESSAGE_MAP(GDIClass, CStatic)
END_MESSAGE_MAP()

void GDIClass::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	Graphics g(lpDrawItemStruct->hDC);
	Bitmap Map(lpDrawItemStruct->rcItem.right - lpDrawItemStruct->rcItem.left + 1,
		lpDrawItemStruct->rcItem.bottom - lpDrawItemStruct->rcItem.top + 1,
		&g);
	Graphics g2(&Map);
	g2.SetSmoothingMode(SmoothingModeAntiAlias);
	Color MyWhite((BYTE)0, (BYTE)0, (BYTE)0);
	g2.Clear(Color::White);
	Matrix matr;
	g2.ResetTransform();


	Gdiplus::REAL kx = (lpDrawItemStruct->rcItem.right - lpDrawItemStruct->rcItem.left) / (double)(xmax - xmin); //���������� ������������
	Gdiplus::REAL ky = (lpDrawItemStruct->rcItem.bottom - lpDrawItemStruct->rcItem.top) / (double)(ymax - ymin);

	Pen pred(Color::Blue, 5.);
	Pen pblack(Color::Black, 1);
	Pen pgray(Color::Gray, 1);
	SolidBrush pstring(Color::Black);
	matr.Scale(kx, ky);
	matr.Scale(1, -1);
	matr.Translate(-xmin, -ymax);
	g2.SetTransform(&matr);

	FontFamily sh(L"Times New Roman");
	Gdiplus::Font shk(&sh, 7);
	wchar_t buf[10];
	if (!auto_manual_GDI)
	{
		for (REAL i = 0; i < ymax; i += (ymax / 10))
		{
			g2.DrawLine(&pgray, xmin, i, xmax, i);
			//����� �� �
			g2.ResetTransform();
			swprintf(buf, 10, L"%.1f", (double)i / scale);
			PointF p(-0.05 * xmax, i);
			matr.TransformPoints(&p);
			g2.DrawString(buf, -1, &shk, p, &pstring);
			g2.SetTransform(&matr);
		}
		for (REAL i = 0; i > ymin; i -= (ymax / 10))
		{
			g2.DrawLine(&pgray, xmin, i, xmax, i);
			//����� �� �
			g2.ResetTransform();
			swprintf(buf, 10, L"%.1f", (double)i / scale);
			PointF p(-0.05 * xmax, i);
			matr.TransformPoints(&p);
			g2.DrawString(buf, -1, &shk, p, &pstring);
			g2.SetTransform(&matr);
		}
	}
	else
	{
		for (REAL i = ymin; i < ymax; i += ((ymax-ymin) / 10))
		{
			g2.DrawLine(&pgray, xmin, i, xmax, i);
			//����� �� �
			g2.ResetTransform();
			swprintf(buf, 10, L"%.1f", (double)i / scale);
			PointF p(-0.05 * xmax, i);
			matr.TransformPoints(&p);
			g2.DrawString(buf, -1, &shk, p, &pstring);
			g2.SetTransform(&matr);
		}
	}
	for (REAL i = 0; i < xmax; i += (xmax / 10))
	{
		g2.DrawLine(&pgray, i, ymin, i, ymax);
	}
	g2.DrawLine(&pblack, (REAL)0, ymin, (REAL)0, ymax); // ���
	g2.DrawLine(&pblack, xmin, (REAL)0, xmax, (REAL)0);

	if (Data.size() > 1)
		for (int i = 1; i < Data.size(); i++)
		{
			g2.DrawLine(&pred, (float)(i - 1), Data[i - 1], (float)i, Data[i]);
		}

	g.DrawImage(&Map, 0, 0);
}
