
// GyroDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Gyro.h"
#include "GyroDlg.h"
#include "afxdialogex.h"
#include <time.h>
#include "math.h"
#include <ctime>
#include <string>
#include <cmath>

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define M_PI 3.1415926535897932384626433832795
#define KOORD_Signal1(x,y) (xp1*((x)-xmin1)),(yp1*((y)-ymax1))   // ��� ��������� ��������� 

// CGyroDlg dialog



CGyroDlg::CGyroDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CGyroDlg::IDD, pParent)
	, fi_0(11)
	, massa(0.6)
	, Massa(3)
	, J(0.006)
	, u(200)
	, L(0.26)
	, ymax_manual(6)
	, ymin_manual(3)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CGyroDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, fi_0);
	DDX_Text(pDX, IDC_EDIT2, massa);
	DDX_Text(pDX, IDC_EDIT3, Massa);
	DDX_Text(pDX, IDC_EDIT4, J);
	DDX_Text(pDX, IDC_EDIT6, u);
	DDX_Text(pDX, IDC_EDIT7, L);
	DDX_Control(pDX, IDOK, Start);
	DDX_Control(pDX, IDC_Signal1, GDIobj); //��������� GDI+
	DDX_Control(pDX, IDC_SLIDER1, AutoSlider);
	DDX_Text(pDX, IDC_EDIT8, ymax_manual);
	DDX_Text(pDX, IDC_EDIT9, ymin_manual);
}

BEGIN_MESSAGE_MAP(CGyroDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CGyroDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CGyroDlg message handlers

BOOL CGyroDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	// ����������� ������� ��� ��������� Signal
	PicWndSignal1 = GetDlgItem(IDC_Signal1);
	PicDcSignal1 = PicWndSignal1->GetDC();
	PicWndSignal1->GetClientRect(&PicSignal1);

	setka_pen.CreatePen(  //��� �����
		//PS_SOLID,    //�������� �����
		PS_DOT,     //����������
		0.2,      //������� 1 �������
		RGB(205, 155, 155));  //���� �����

	osi_pen.CreatePen(   //������������ ���
		PS_SOLID,    //�������� �����
		3,      //������� 3 �������
		RGB(138, 138, 138));   //���� ������

	graf_pen.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(0, 0, 0));   //���� ������

	graf_pen1.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(205, 275, 205));   //���� �������

	graf_penred.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(255, 0, 0));   //���� �������

	graf_pengreen.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(0, 150, 0));   //���� �������

	graf_penblue.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(0, 0, 255));   //���� �������

	xmin1 = 0;			//����������� �������� �1
	xmax1 = 10;			//������������ �������� �1
	ymin1 = -1;			//����������� �������� y1
	ymax1 = 1;			//������������ �������� y1

	delta_t = 0.01;
	FI = fi_0 * 2 * M_PI;
	FI_DT = 0;
	g = 9.7;
	t = 0;
	g = 9.7;
	Faza.clear();
	Signal.clear();
	AutoSlider.SetRangeMax(1);
	AutoSlider.SetRangeMin(0);
	AutoSlider.SetPos(0);
	OdnaKnopka = 1;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CGyroDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		CString str; // �������
		xp1 = ((double)(PicSignal1.Width()) / (xmax1 - xmin1));
		yp1 = -((double)(PicSignal1.Height()) / (ymax1 - ymin1));

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//���� �������
		PicDcSignal1->FillSolidRect(&PicSignal1, RGB(255, 255, 255)); //����������� �� ������
		PicDcSignal1->SelectObject(&osi_pen);		     //�������� �����

		//������ ��� �������
		PicDcSignal1->MoveTo(KOORD_Signal1(xmin1, ymin1));
		PicDcSignal1->LineTo(KOORD_Signal1(xmin1, ymax1));

		//������ ��� t
		PicDcSignal1->MoveTo(KOORD_Signal1(xmin1, 0));
		PicDcSignal1->LineTo(KOORD_Signal1(xmax1, 0));

		PicDcSignal1->SelectObject(&setka_pen);
		for (int i = xmin1; i <= xmax1; i = i + (xmax1 / 10)) //���������
		{
			PicDcSignal1->MoveTo(KOORD_Signal1(i, ymin1));
			PicDcSignal1->LineTo(KOORD_Signal1(i, ymax1));
		}
		for (double i = ymin1; i <= ymax1; i = i + (ymax1 / 5)) //����������� 
		{
			PicDcSignal1->MoveTo(KOORD_Signal1(xmin1, i));
			PicDcSignal1->LineTo(KOORD_Signal1(xmax1, i));
		}

		str.Format(_T("%.0f, Fi(t)"), ymax1);
		PicDcSignal1->TextOutW(KOORD_Signal1(0, ymax1), str);

		PicDcSignal1->TextOutW(KOORD_Signal1(xmax1 - 0.05 * ymax1, -0.05 * ymax1), _T("t"));     //X

		PicDcSignal1->SelectObject(&graf_pen);
		PicDcSignal1->MoveTo(KOORD_Signal1(0, xmin1));
		int k = 0;
		for each (double e in Signal)
		{
			PicDcSignal1->LineTo(KOORD_Signal1(delta_t * (k++) + xmin1, e));
		}
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CGyroDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

double CGyroDlg::func(double t, double phi, double d_phi)
{
	return (u * t + ((Massa + massa) * (g * sin(phi))) / (cos(phi)) - massa * L * d_phi * d_phi * sin(phi)) / (((Massa + massa) * (J + massa * L * L)) / (massa * L * cos(phi)) - massa * L * cos(phi));
}

void CGyroDlg::OnBnClickedOk()// ����
{
	//CDialogEx::OnOK();

	UpdateData(TRUE);
	xmin1 = 0;
	xmax1 = 10;
	ymin1 = -1;
	ymax1 = 1;

	Signal.clear();
	FI = M_PI / 2;
	FI_DT = fi_0 * M_PI / 180;
	t = 0;
	UpdateData(FALSE);

	switch (OdnaKnopka)
	{
	case 1:
		OdnaKnopka = 2;
		SetTimer(1, 20, NULL);
		Start.SetWindowText((LPCWSTR)L"����");
		break;

	case 2:
		KillTimer(1);
		Start.SetWindowText((LPWSTR)L"����");
		OdnaKnopka = 1;
		break;
	}


	UpdateData(FALSE);
}

void CGyroDlg::OnTimer(UINT_PTR nIDEvent)
{
	Runge_Kutt(FI, FI_DT, t);
	//Runge_Kutt(FI, t);

	double X = FI_DT;
	Signal.push_back(X);
	if (X > ymax1)
		ymax1 = X;
	if (X < ymin1)
		ymin1 = X;
	if (Signal.size() * delta_t > xmax1)
		Signal.erase(Signal.begin());
	GDIobj.GDIPaint(Signal, ymin_manual, ymax_manual, AutoSlider.GetPos());
	//OnPaint();
}
double CGyroDlg::Runge_Kutt(double& ugol, double& v_ugol, double& time)
{
	double x1_0 = ugol;
	double V1_0 = v_ugol;
	double dtt = delta_t;
	double k1_x1 = func(t, x1_0, V1_0) * dtt;
	double k2_x1 = func(t, x1_0 + V1_0 * dtt / 2, V1_0 + k1_x1 / 2) * dtt;
	double k3_x1 = func(t, x1_0 + V1_0 * dtt / 2 + k1_x1 * dtt / 4, V1_0 + k2_x1 / 2) * dtt;
	double k4_x1 = func(t, x1_0 + V1_0 * dtt + k2_x1 * dtt / 2, V1_0 + k3_x1) * dtt;
	ugol = x1_0 + V1_0 * dtt + 1. / 6 * (k1_x1 + k2_x1 + k3_x1) * dtt;
	v_ugol = V1_0 + 1. / 6 * (2 * k2_x1 + 2 * k3_x1 + k1_x1 + k4_x1);
	time += dtt;
	return 0;
}




