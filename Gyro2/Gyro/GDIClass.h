#pragma once
#include <afxwin.h>
#include <math.h>
#include <vector>
using namespace std;
using namespace Gdiplus;
#define M_PI 3.1415926535
class GDIClass :
	public CStatic
{
	//////////////////////////////////////////
	DECLARE_DYNAMIC(GDIClass)
public:
	GDIClass();
	virtual ~GDIClass();
	virtual void DrawItem(LPDRAWITEMSTRUCT);
private:
protected:
	DECLARE_MESSAGE_MAP()
	//////////////////////////////////////////
public:
	REAL xmax = 1000;
	REAL xmin = -0.1 * 1000;
	REAL ymax = 100;
	REAL ymin = -0.1 * ymax;
	REAL scale = 100;
	vector<REAL> Data;
	bool auto_manual_GDI = 0;

	void GDIPaint(vector<double> AAA, double ymin_manual, double ymax_manual, int auto_manual)
	{

		Data.clear();
		if (AAA.size() > 1)
			for (int i = 0; i < AAA.size(); i++)
			{
				AAA[i] *= scale;
				if (AAA[i] > ymax)ymax = AAA[i];
				if ((AAA[i] < ymin))ymin = AAA[i];
				if (ymax > (-ymin)) ymin = -ymax; else ymax = -ymin;
				Data.push_back((REAL)AAA[i]);
				auto_manual_GDI = 0;
			}
		else
		{
			ymax = 100;
			ymin = -0.1 * ymax;
		}
		if (auto_manual == 1)
		{
			ymin = ymin_manual * scale;
			ymax = ymax_manual * scale;
			auto_manual_GDI = 1;
		}
		Invalidate();
	}

};

