
// GyroDlg.h : header file
//
#include <vector>
#include "afxwin.h"
#include "GDIClass.h"

#pragma once
using namespace std;

// CGyroDlg dialog
class CGyroDlg : public CDialogEx
{
	// Construction
public:
	CGyroDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_GYRO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:

	double xp1, yp1,        // ����������� ���������
		xmin1, xmax1,	    // �������������� � ����������� �������� ������ ������
		ymin1, ymax1;       // �������������� � ����������� �������� ������ ������

	// ������� ������ CWnd
	CWnd* PicWndSignal1;

	// ������� ������ CDC
	CDC* PicDcSignal1;
	// ������� ������ CRect
	CRect PicSignal1;

	//�����
	CPen osi_pen;
	CPen setka_pen;
	CPen graf_pen;
	CPen graf_pen1;
	CPen graf_penred;
	CPen graf_pengreen;
	CPen graf_penblue;

	double L; // ����� 
	double massa; // �����
	double Massa; // ����� 
	double g; // ��������� ���������� �������
	double J; // ������ ������� ��������
	double b; // ����� ������
	double u; // ����������� �����������
	double t;
	double X_begin; // ���������� ����� �������
	double fi_0; // ��� ����������
	double FI; // ��������� ���������� � ��������
	double FI_DT; // ������ �����������	
	double delta_t; // ��� �� �������
	int OdnaKnopka;
	struct Dot
	{
		double fi1, fi1_dt1;
	};
	vector <Dot> Faza;
	//double Runge_Kutt(double &ugol, double &v_ugol, double  &time);   // ����� ����� �����
	double Runge_Kutt(double& ugol, double& v_ugol, double& time);   // ����� ����� �����
	double func(double t, double phi, double d_phi);        // �������
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	UINT_PTR timer_ptr;      // ������ �������
	vector <double> Signal;
	afx_msg void OnBnClickedOk();
	CButton Start;
	////////////////////////////
	GDIClass GDIobj; //GDICLASS
	CSliderCtrl AutoSlider;
	double ymax_manual;
	double ymin_manual;
};
