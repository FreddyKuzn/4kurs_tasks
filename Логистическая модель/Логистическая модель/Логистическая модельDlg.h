
// ������������� ������Dlg.h : header file
//
#include <vector>
#include <math.h>
#include <time.h>
#include "afxwin.h"
using namespace std;

#pragma once


// C�������������������Dlg dialog
class C�������������������Dlg : public CDialogEx
{
// Construction
public:
	C�������������������Dlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MY_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
	double xp1, yp1,        // ����������� ���������
		xmin1, xmax1,	    // �������������� � ����������� �������� ������ ������
		ymin1, ymax1;       // �������������� � ����������� �������� ������ ������

	double xp2, yp2,        // ����������� ���������
		xmin2, xmax2,	    // �������������� � ����������� �������� ������ ������
		ymin2, ymax2;       // �������������� � ����������� �������� ������ ������

	// ������� ������ CWnd
	CWnd * PicWndSignal1;
	CWnd * PicWndSignal2;

	// ������� ������ CDC
	CDC * PicDcSignal1;
	CDC * PicDcSignal2;

	// ������� ������ CRect
	CRect PicSignal1;
	CRect PicSignal2;

	//�����
	CPen osi_pen;
	CPen setka_pen;
	CPen graf_pen;
	CPen graf_pen1;
	CPen graf_penred;
	CPen graf_pengreen;
	CPen graf_penblue;

	
	double *Signal1; //������ �������
	double *Signal2; //������ ����������
	
	int bx, by;
	
	double R; // �������� R
	double x_0; // ���. �����������
	int N; // ���-�� ��������
	int p; // ���-�� ������ ����
	int Kol_iter; 	// ���-�� ��������
	double Sign;

	//////////////////////////////////////////////////////////////////////////////////
	struct Dot                             //�����
	{
		double x;
		double y;                           //����������     


	};
	struct Node                             //����� ������
	{
		double x;
		double y;                          //��, ��� ���������� � ������
		vector<Dot> koord_add_dots;
		double deep;
	};
	vector <Node> Buffer;

	//void del(Node *&Tree);	/*�������� ������� ������*/
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();	
	afx_msg void OnBnClickedButton3();
	//Node* add_node_new(double x, double y, Node *&MyTree, double acc) //������ ���������� ����� � ������
	//{
	//	if (NULL == MyTree)             //��, � ��� � � ����� ������ �����. ���� ������ ���, �� ����� �������
	//	{
	//		MyTree = new Node;          //�������� ������ ��� ����� ������
	//		MyTree->x = x;
	//		MyTree->y = y; //���������� ������ � �����
	//		MyTree->l = MyTree->r = NULL; //��������� �������������� �������� �� ��������� ������
	//	}

	//	else
	//	{
	//		if (y > MyTree->y)
	//		{
	//			if (abs(x - MyTree->x) <= acc)
	//			{

	//				MyTree->r = add_node_new(x, y, MyTree->r, acc);
	//			}
	//			else
	//			{
	//				if (MyTree->r != NULL)
	//				{
	//					MyTree->r = add_node_new(x, y, MyTree->r, acc);
	//				}
	//				else MyTree->l = add_node_new(x, y, MyTree->l, acc);
	//			}


	//		}
	//		else
	//		{
	//			if (abs(x - MyTree->x) <= acc)
	//			{
	//				MyTree->l = add_node_new(x, y, MyTree->l, acc);
	//			}
	//			else
	//			{
	//				if (MyTree->l != NULL)
	//				{
	//					MyTree->l = add_node_new(x, y, MyTree->l, acc);
	//				}
	//				else MyTree->r = add_node_new(x, y, MyTree->r, acc);
	//			}
	//		}
	//	}
	//	return MyTree;
	//}
	Node *TreeTrue;
	void paintTreeLine(vector <Dot> BuffPaint);           //������� ������
	void BufferTree(Node *&Tree);           //������� ������
	bool OnOff;
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
};
