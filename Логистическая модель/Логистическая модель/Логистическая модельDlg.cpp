
// ������������� ������Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "������������� ������.h"
#include "������������� ������Dlg.h"
#include "afxdialogex.h"
#include <time.h>
#include"math.h"
#include <fstream>
#include <fsrmtlb.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// C�������������������Dlg dialog
#define KOORD_Signal1(x,y) (xp1*((x)-xmin1)),(yp1*((y)-ymax1))   // ��� ��������� ��������� 
#define KOORD_Signal2(x,y) (xp2*((x)-xmin2)),(yp2*((y)-ymax2))   // ��� ��������� ��������� 
#define KOORD_Signal2_2(x,y) (xp2*((x)-xmin2))+2,(yp2*((y)-ymax2))+2   // ��� ��������� ��������� 


C�������������������Dlg::C�������������������Dlg(CWnd* pParent /*=NULL*/)
: CDialogEx(C�������������������Dlg::IDD, pParent)
, R(1)
, x_0(1)
, N(30)
, p(1000)
, Kol_iter(100)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void C�������������������Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, R);
	DDV_MinMaxInt(pDX, R, 0, 4);
	DDX_Text(pDX, IDC_EDIT2, x_0);
	DDV_MinMaxInt(pDX, x_0, 0, 100000000);
	DDX_Text(pDX, IDC_EDIT3, N);
	DDV_MinMaxInt(pDX, N, 0, 10000);
	DDX_Text(pDX, IDC_EDIT4, p);
	DDV_MinMaxInt(pDX, p, 0, 10000);
	DDX_Text(pDX, IDC_EDIT5, Kol_iter);
	DDV_MinMaxInt(pDX, Kol_iter, 0, 1000);
}

BEGIN_MESSAGE_MAP(C�������������������Dlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &C�������������������Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &C�������������������Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &C�������������������Dlg::OnBnClickedButton3)
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()


// C�������������������Dlg message handlers

BOOL C�������������������Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	// ����������� ������� ��� ��������� Signal
	PicWndSignal1 = GetDlgItem(IDC_Signal1);
	PicDcSignal1 = PicWndSignal1->GetDC();
	PicWndSignal1->GetClientRect(&PicSignal1);

	// ����������� ������� ��� ��������� Signal
	PicWndSignal2 = GetDlgItem(IDC_Signal2);
	PicDcSignal2 = PicWndSignal2->GetDC();
	PicWndSignal2->GetClientRect(&PicSignal2);

	setka_pen.CreatePen(  //��� �����
		//PS_SOLID,    //�������� �����
		PS_DOT,     //����������
		1,      //������� 1 �������
		RGB(205, 155, 155));  //���� �����

	osi_pen.CreatePen(   //������������ ���
		PS_SOLID,    //�������� �����
		2,      //������� 3 �������
		RGB(0, 0, 0));   //���� ������

	graf_pen.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(0, 0, 0));   //���� ������
	graf_penred.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(255, 0, 0));   //���� ������
	graf_pengreen.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(0, 150, 0));   //���� ������
	graf_penblue.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(0, 0, 255));   //���� ������
	UpdateData(false);

	xmin1 = 0;			//����������� �������� �1
	xmax1 = N;			//������������ �������� �1
	ymin1 = -1;
	ymax1 = 1;

	xmin2 = 0;
	xmax2 = 4;
	ymin2 = -0.18;
	ymax2 = 1;
	bx = by = 0;
	xp1 = ((double)(PicSignal1.Width()) / (xmax1 - xmin1));			// ������������ ��������� ��������� ������ ������
	yp1 = -((double)(PicSignal1.Height()) / (ymax1 - ymin1));       // ������������ ��������� ��������� ������ ������

	xp2 = ((double)(PicSignal2.Width()) / (xmax2 - xmin2));			// ������������ ��������� ��������� 
	yp2 = -((double)(PicSignal2.Height()) / (ymax2 - ymin2));       // ������������ ��������� ���������

	Signal1 = new double[10000];

	for (int i = 0; i < 10000; i++)
	{
		Signal1[i] = 0;
	}
	TreeTrue = NULL;
	Buffer.clear();
	OnOff = false;
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void C�������������������Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		CString str; // �������
		xp1 = ((double)(PicSignal1.Width()) / (xmax1 - xmin1));
		yp1 = -((double)(PicSignal1.Height()) / (ymax1 - ymin1));
		xp2 = ((double)(PicSignal2.Width()) / (xmax2 - xmin2));
		yp2 = -((double)(PicSignal2.Height()) / (ymax2 - ymin2));


		//���� �������
		PicDcSignal1->FillSolidRect(&PicSignal1, RGB(255, 255, 255)); //����������� �� ������
		PicDcSignal1->SelectObject(&osi_pen);		     //�������� �����
		//������ ��� Signal
		PicDcSignal1->MoveTo(KOORD_Signal1(xmin1, ymin1));
		PicDcSignal1->LineTo(KOORD_Signal1(xmin1, ymax1));
		//������ ��� t
		PicDcSignal1->MoveTo(KOORD_Signal1(xmin1, 0));
		PicDcSignal1->LineTo(KOORD_Signal1(xmax1, 0));
		PicDcSignal1->SelectObject(&setka_pen);
		for (int i = xmin1; i <= xmax1; i = i + (N / 10)) //���������
		{
			PicDcSignal1->MoveTo(KOORD_Signal1(i, ymin1));
			PicDcSignal1->LineTo(KOORD_Signal1(i, ymax1));
		}
		for (double i = 0; i <= ymax1; i = i + (ymax1 / 5)) //����������� ��� ���� t
		{
			PicDcSignal1->MoveTo(KOORD_Signal1(xmin1, i));
			PicDcSignal1->LineTo(KOORD_Signal1(xmax1, i));
		}
		for (double i = ymax1 / 5; i >= ymin1; i = i - (ymax1 / 5)) //����������� ��� ���� t
		{
			PicDcSignal1->MoveTo(KOORD_Signal1(xmin1, i));
			PicDcSignal1->LineTo(KOORD_Signal1(xmax1, i));
		}
		for (double i = xmin1; i <= xmax1; i = i + (N / 2))
		{

			str.Format(_T("%.0f"), i);
			PicDcSignal1->TextOutW(KOORD_Signal1(i - 0 * (ymax1 - ymin1), 0 - 0.02* (ymax1 - ymin1)), str);
		}
		str.Format(_T("%.0f"), ymax1);
		PicDcSignal1->TextOutW(KOORD_Signal1(0, ymax1), str);
		str.Format(_T("%.0f"), ymin1);
		PicDcSignal1->TextOutW(KOORD_Signal1(0, ymin1 + 0.1*(ymax1 - ymin1)), str);
		PicDcSignal1->MoveTo(KOORD_Signal1(0, Signal1[0]));
		PicDcSignal1->SelectObject(graf_penblue);
		for (int i = 0; i < N + 1; i++)
		{
			PicDcSignal1->LineTo(KOORD_Signal1(i, Signal1[i]));

		}


		//���� ���������
		PicDcSignal2->FillSolidRect(&PicSignal2, RGB(255, 255, 255)); //����������� �� ������
		PicDcSignal2->SelectObject(&osi_pen);		     //�������� �����
		//������ ��� Signal
		PicDcSignal2->MoveTo(KOORD_Signal2(xmin2, ymin2));
		PicDcSignal2->LineTo(KOORD_Signal2(xmin2, ymax2));
		//������ ��� t
		PicDcSignal2->MoveTo(KOORD_Signal2(xmin2, 0));
		PicDcSignal2->LineTo(KOORD_Signal2(xmax2, 0));
		PicDcSignal2->SelectObject(&setka_pen);
		for (int i = xmin2; i <= xmax2; i = i + (4 / 4)) //���������
		{
			PicDcSignal2->MoveTo(KOORD_Signal2(i, 0));
			PicDcSignal2->LineTo(KOORD_Signal2(i, ymax2));
		}
		for (double i = 0; i <= ymax2; i = i + (ymax2 / 5)) //����������� ��� ���� t
		{
			PicDcSignal2->MoveTo(KOORD_Signal2(xmin2, i));
			PicDcSignal2->LineTo(KOORD_Signal2(xmax2, i));
		}

		for (double i = xmin2; i <= xmax2; i = i + (4 / 4))
		{

			str.Format(_T("%.0f"), i);
			PicDcSignal2->TextOutW(KOORD_Signal2(i - 0 * (ymax2 - ymin2), 0 - 0.02* (ymax2 - ymin2)), str);
		}
		str.Format(_T("%.0f"), ymax2);
		PicDcSignal2->TextOutW(KOORD_Signal2(0, ymax2), str);

		if (Buffer.size() != 0)
		{

			if (OnOff)
			{
				PicDcSignal2->SelectObject(graf_penred);
				for (int i = 0; i < Buffer.size(); i++)
				{
					for (int j = 0; j < Buffer[i].koord_add_dots.size(); j++)
					{
						PicDcSignal2->MoveTo(KOORD_Signal2(Buffer[i].x,Buffer[i].y));
						PicDcSignal2->LineTo(KOORD_Signal2(Buffer[i].koord_add_dots[j].x, Buffer[i].koord_add_dots[j].y));
					}
				}
			}
			else
			{
				PicDcSignal2->SelectObject(graf_penblue);
				for (int i = 0; i < Buffer.size(); i++)
				{
					PicDcSignal2->Ellipse(KOORD_Signal2(Buffer[i].x, Buffer[i].y), KOORD_Signal2_2(Buffer[i].x, Buffer[i].y));
				}
			}





		}

		/*PicDcSignal2->MoveTo(KOORD_Signal2(0, Signal2[0]));
		PicDcSignal2->SelectObject(graf_penblue);
		for (int i = 0; i < 5; i++)
		{
		PicDcSignal2->LineTo(KOORD_Signal2(i, Signal2[i]));


		}*/
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR C�������������������Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void C�������������������Dlg::OnBnClickedButton1() // ��������� �������
{
	UpdateData(TRUE);
	xmin1 = 0;
	xmax1 = N;
	ymax1 = 1;
	ymin1 = -1;

	for (int i = 0; i < 10000; i++)
	{
		Signal1[i] = 0;
	}

	for (int i = 0; i < N; i++)
	{
		Signal1[0] = x_0;
		Signal1[i + 1] += R*(1 - Signal1[i]);
	}


	ymax1 = 0;
	for (int k = 0; k < N; k++)
	{
		if (Signal1[k]>ymax1)
			ymax1 = Signal1[k];
	}
	ymin1 = 0;
	for (int k = 0; k < N; k++)
	{
		if (Signal1[k] < ymin1)
			ymin1 = Signal1[k] - 1;
	}

	Invalidate();
	UpdateData(FALSE);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void C�������������������Dlg::OnBnClickedButton2() // ��������� ���������
{
	UpdateData(TRUE);
	OnOff = false;
	xmin2 = 0;
	xmax2 = 4;
	ymax2 = 1;
	ymin2 = -0.18;
	Buffer.clear();

	double chag_R = (4. - 0.) / 150.;

	for (double i = 0; i < 4; i += chag_R)
	{
		Signal2 = new double[Kol_iter];
		int c4et = 0;
		for (int j = 0; j < Kol_iter; j++)
		{
			Signal2[j] = 0;
		}
		for (int k = 0; k < Kol_iter; k++)
		{
			bool sovpadenie = false;
			double x_0_0 = 0 + 1 * rand()*1. / RAND_MAX;
			for (int g = 1; g < p; g++)
			{
				x_0_0 = i* x_0_0*(1 - x_0_0);
			}
			for (int m = 0; m < c4et; m++)
			{
				if (abs(Signal2[m] - x_0_0) < 0.001) sovpadenie = true;
				if (abs(x_0_0) < 0.001) sovpadenie = true;
			}
			if (!sovpadenie)
			{
				Node GGG; GGG.x = i; GGG.y = x_0_0;
				Buffer.push_back(GGG);
				Signal2[c4et] = x_0_0;
				c4et++;
			}
		}
		delete[] Signal2;

	}

	Invalidate();
	UpdateData(FALSE);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void C�������������������Dlg::OnBnClickedButton3() // ��������� �����
{
	UpdateData(TRUE);
	OnOff = true;
	Buffer[0].deep = 0;
	double shug = (4. - 0.) / (150.);
	for (int i = 1; i < Buffer.size(); i++)
	{
		double Big = 1000;
		int number = NULL;
		double sq = 1000;
		int iter_nearest = 0;
		for (int j = 0; j < i; j++)
		{
			double a = Buffer[i].x;
			double b = Buffer[j].x;
			if (((Buffer[i].x - Buffer[j].x) <= 1.5*shug) && ((Buffer[i].x - Buffer[j].x)>=0.5*shug))
			{
				
				double nsq = sqrt(pow((Buffer[i].x - Buffer[j].x), 2) + pow((Buffer[i].y - Buffer[j].y), 2));
				if (sq > nsq)
				{
					sq = nsq;
					number = j;
				}
				/*double dd = Buffer[j].deep + sq;
				if (dd < Big)
				{
					number = j;//����� ����������� ������� ������ � ����������� ������ 
					Big = dd;
				}*/
			}
		}
		Buffer[i].deep = Buffer[number].deep + sqrt((Buffer[i].x - Buffer[number].x)*(Buffer[i].x - Buffer[number].x) + (Buffer[i].y - Buffer[number].y)*(Buffer[i].y - Buffer[number].y));
		Dot ss; ss.x = Buffer[i].x; ss.y = Buffer[i].y;
		Buffer[number].koord_add_dots.push_back(ss);
			
	}

	Invalidate();
	UpdateData(FALSE);
}


void C�������������������Dlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: �������� ���� ��� ����������� ��������� ��� ����� ������������
	if (nFlags && ((int)point.x > bx))
	{
		double f = (xmax2 - xmin2)*0.01;
		xmin2 = xmin2 - f;
		xmax2 = xmax2 - f;
		bx = (int)point.x;
		OnPaint();
	}
	if (nFlags && ((int)point.x < bx))
	{
		double f = (xmax2 - xmin2)*0.01;
		xmin2 = xmin2 + f;
		xmax2 = xmax2 + f;
		bx = (int)point.x;
		OnPaint();
	}
	if (nFlags && ((int)point.y > by))
	{
		double f = (ymax2 - ymin2)*0.01;
		ymin2 = ymin2 + f;
		ymax2 = ymax2 + f;
		by = (int)point.y;
		OnPaint();
	}
	if (nFlags && ((int)point.y < by))
	{
		double f = (ymax2 - ymin2)*0.01;
		ymin2 = ymin2 - f;
		ymax2 = ymax2 - f;
		by = (int)point.y;
		OnPaint();
	}
	CDialogEx::OnMouseMove(nFlags, point);
}


BOOL C�������������������Dlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: �������� ���� ��� ����������� ��������� ��� ����� ������������
	if (nFlags && (zDelta > 0))
	{

		xmax2 = xmax2 * 0.9;
		xmin2 = xmin2 * 0.9;
		ymax2 = ymax2 * 0.9;
		ymin2 = ymin2 * 0.9;
		OnPaint();
	}
	if (nFlags && (zDelta < 0))
	{

		xmax2 = xmax2 * 1.1;
		xmin2 = xmin2 * 1.1;
		ymax2 = ymax2 * 1.1;
		ymin2 = ymin2 * 1.1;
		OnPaint();
	}
	return CDialogEx::OnMouseWheel(nFlags, zDelta, pt);
}
