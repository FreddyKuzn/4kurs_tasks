
// TeskEdDlg.cpp: файл реализации
//

#include "stdafx.h"
#include "TeskEd.h"
#include "TeskEdDlg.h"
#include "afxdialogex.h"
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define KOORD(x,y) (xp1*((x)-xmin1)),(yp1*((y)-ymax1)) 

// Диалоговое окно CTeskEdDlg



CTeskEdDlg::CTeskEdDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TESKED_DIALOG, pParent)
	, Nd(7e+016)
	, LEdit(200)
	, E(4)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTeskEdDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, Nd);
	DDX_Text(pDX, IDC_EDIT2, LEdit);
	DDX_Text(pDX, IDC_EDIT3, E);
}

BEGIN_MESSAGE_MAP(CTeskEdDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CTeskEdDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CTeskEdDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// Обработчики сообщений CTeskEdDlg

BOOL CTeskEdDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	PicWndMult = GetDlgItem(IDC_TESK);
	PicDcMult = PicWndMult->GetDC();
	PicWndMult->GetClientRect(&PicMult);
	setka_pen.CreatePen(  //для сетки
		PS_DASH,    //сплошная линия
		//PS_DOT,     //пунктирная
		1,      //толщина 1 пиксель
		RGB(0, 0, 0));  //цвет серый

	osi_pen.CreatePen(   //координатные оси
		PS_SOLID,    //сплошная линия
		3,      //толщина 3 пикселя
		RGB(138, 138, 138));   //цвет черный

	graf_pen.CreatePen(   //график
		PS_SOLID,    //сплошная линия
		12,      //толщина 2 пикселя
		RGB(0, 0, 0));   //цвет черный

	graf_pen1.CreatePen(   //график
		PS_SOLID,    //сплошная линия
		1,      //толщина 2 пикселя
		RGB(0, 0, 0));   //цвет розовый

	graf_penred.CreatePen(   //график
		PS_SOLID,    //сплошная линия
		1,      //толщина 2 пикселя
		RGB(255, 0, 0));   //цвет черный

	graf_pengreen.CreatePen(   //график
		PS_SOLID,    //сплошная линия
		4,      //толщина 2 пикселя
		RGB(0, 150, 0));   //цвет черный

	graf_penblue.CreatePen(   //график
		PS_SOLID,    //сплошная линия
		1,      //толщина 2 пикселя
		RGB(0, 0, 255));   //цвет черный



	xmax1 = 10;			//Максимальное значение х1
	ymax1 = 5;			//Максимальное значение y1
	xmin1 = -0.1*xmax1;			//Минимальное значение х1
	ymin1 = -0.1*ymax1;			//Минимальное значение y1
	dotscount = 500;

	nd = 10;
	Ld = sqrt((double)(epsi * epsi0 * k * Temp) / (qe * qe * Nd * (nd)));
	L = LEdit * 1e-6 / Ld;
	step = L / dotscount;




	return TRUE;  // return TRUE  unless you set the focus to a control
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CTeskEdDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		///////////////////////////////////////////////
		///////////////////////////////////////////////
		xp1 = ((double)(PicMult.Width()) / (xmax1 - xmin1));			// Коэффициенты пересчёта координат по x1
		yp1 = -((double)(PicMult.Height()) / (ymax1 - ymin1));       // Коэффициенты пересчёта координат по y1
		///////////////////////////////////////////////
		// для MULT
		PicDcMult->FillSolidRect(&PicMult, RGB(200, 200, 200));    // закрашиваем фон
		PicDcMult->SelectObject(&osi_pen);		// выбираем ручку

		//создаём Ось Y
		PicDcMult->MoveTo(KOORD(0, ymin1));
		PicDcMult->LineTo(KOORD(0, ymax1));
		//создаём Ось Х
		PicDcMult->MoveTo(KOORD(xmin1, 0));
		PicDcMult->LineTo(KOORD(xmax1, 0));

		
		if (graphic.size() != NULL)
		{
			PicDcMult->SelectObject(&graf_penblue);
			PicDcMult->MoveTo(KOORD(0, graphic[0]));
			for (int k = 0; k < dotscount; k++)
			{
				PicDcMult->LineTo(KOORD(k, graphic[k]));
			}
		}
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CTeskEdDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}




double CTeskEdDlg::F(double pot)
{
	return (-nd * (1.0 - exp(-pot)));
}

double CTeskEdDlg::q(double pot)
{
	return -(nd * exp(-pot));
}

double CTeskEdDlg::r(double pot)
{
	return F(pot) + q(pot) * pot;
}


void CTeskEdDlg::OnBnClickedOk() //Play
{
	UpdateData(TRUE);

	graphic.clear();
	Ld = sqrt((double)(epsi * epsi0 * k * Temp) / (qe * qe * Nd * (nd)));
	L = LEdit * 1e-6 / Ld;
	step = L / dotscount;


	Edge.size = dotscount;
	Edge.left = E;
	Edge.right = E;
	Edge.length = LEdit;
	Edge.nA = 0;
	Edge.nD = 10;
	Edge.n0 = Nd;
	Edge.eps = error;
	Edge.is_left_field = true;
	Edge.is_right_field = false;
	Edge.is_vach = false;
	Edge.is_vfch = false;
	Edge.is_phch = false;
	Buffer =  new SemiconductorPlateSolver(Edge);
	Buffer->solve(graphic,vector<double>(),iter);



	xmax1 = dotscount;
	xmin1 = 0;			//Минимальное значение х1
	ymax1 = 0;
	for (int i = 0; i < dotscount; i++)
		if (graphic[i] > ymax1)ymax1 = graphic[i];

	Invalidate();
	UpdateData(FALSE);
}

void CTeskEdDlg::Function(double E)
{
	alpha.clear();
	beta.clear();
	yy.clear();
	yy_past.clear();
	for (int i = 0; i < dotscount; i++)
	{
		alpha.push_back(0);
		beta.push_back(0);
		yy.push_back(0);
		yy_past.push_back(0);
	}

	int iter = 0;
	double curr_eps = 0;

	do
	{
		for (int i = 0; i < dotscount; i++) yy_past[i] = yy[i];

		alpha[0] = 0;
		beta[0] = E;

		for (int i = 1; i < dotscount; i++)
		{
			alpha[i] = alpha[i - 1] + (-q(yy_past[i - 1]) - alpha[i - 1] * alpha[i - 1]) * step;
			beta[i] = beta[i - 1] - (-r(yy_past[i - 1]) + alpha[i - 1] * beta[i - 1]) * step;
		}
		yy[dotscount - 1] = (E - beta[dotscount - 1]) / (alpha[dotscount - 1]);

		for (int i = dotscount - 2; i >= 0; i--)
		{
			yy[i] = yy[i+1] -(alpha[i + 1] * yy[i + 1] + beta[i + 1])*step;
		}
		curr_eps = count_difference();
		iter++;
	} while (curr_eps > error);
	ymin1 = 0;
	for (int i = 0; i < yy.size(); i++)
	{
		if (yy[i] < ymin1) ymin1 = yy[i];
	}
}

double CTeskEdDlg::count_difference()
{
	double summ = 0;
	double der = 0;
	for (int i = 0; i < dotscount; i++)
	{
		summ += (yy[i] - yy_past[i]) * (yy[i] - yy_past[i]);
		der += yy[i] * yy[i];
	}

	return summ / der;
}


void CTeskEdDlg::OnBnClickedCancel()
{
	// TODO: добавьте свой код обработчика уведомлений
	CDialogEx::OnCancel();
}
