
// TeskEdDlg.h: файл заголовка
////
#include <vector>
#include <list>
#include "SemiconductorPlateSolver.h"
#pragma once
using namespace std;

// Диалоговое окно CTeskEdDlg
class CTeskEdDlg : public CDialogEx
{
// Создание
public:
	CTeskEdDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TESKED_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	double Nd;
	double LEdit;
	double E;
	//Переменные рисования
// объекты класса CWnd
	CWnd * PicWndMult;
	// объекты класса CDC
	CDC * PicDcMult;
	// объекты класса CRect
	CRect PicMult;
	//Переменные для работы с масштабом
	double xp1, yp1,        // коэфициенты пересчета
		xmin1, xmax1,	    // максисимальное и минимальное значение х1 
		ymin1, ymax1;       // максисимальное и минимальное значение y1
		//ручки
	CPen osi_pen;
	CPen setka_pen;
	CPen graf_pen;
	CPen graf_pen1;
	CPen graf_penred;
	CPen graf_pengreen;
	CPen graf_penblue;
	///////////////////////////////////////////////
	///////////////////////////////////////////////
	double qe = -1.6e-19,  //заряд
		epsi = 12,          //диэлектрическая проницаемость в Si
		epsi0 = 8.86e-12,   //диэлектрическая проницаемость в вакууме
		Temp = 300,         //температура
		k = 1.38064852e-23,     //постоянная Больцмана
		fi_const = 0.026,
		L,
		nd,     //относительная
		Ld,     //длина Дебая             
		step,               //шаг по толщине
		error = 1e-5;       //ошибка
	int dotscount;


	struct Dot
	{
		double x, y;
	};
	vector <double> yy;
	vector <double> graphic;
	vector <double> yy_past;
	vector <double> alpha;
	vector <double> beta;
	///////////////////////////////////////////////
	///////////////////////////////////////////////
	//Функции
	double F(double pot);
	double q(double pot);
	double r(double pot);
	void Function(double E);
	Problem Edge;
	SemiconductorPlateSolver *Buffer;
	int iter;
private:
	double count_difference();
};
