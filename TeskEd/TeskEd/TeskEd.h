
// TeskEd.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить stdafx.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CTeskEdApp:
// Сведения о реализации этого класса: TeskEd.cpp
//

class CTeskEdApp : public CWinApp
{
public:
	CTeskEdApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CTeskEdApp theApp;
