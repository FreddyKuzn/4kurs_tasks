
// ModSystemZ1(kapitza)Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "ModSystemZ1(kapitza).h"
#include "ModSystemZ1(kapitza)Dlg.h"
#include "afxdialogex.h"
#include "locale.h"
#include <time.h>
#include "math.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// �������� ���������� ��� �������
#define KOORD(x,y) (xp1*((x)-xmin1)),(yp1*((y)-ymax1))          // ��� ��������� ��������� Mult
#define KOORD_1_1(x,y) (xp1*((x)-xmin1)-7),(yp1*((y)-ymax1)-7) //��� �������    
#define KOORD_1_2(x,y) (xp1*((x)-xmin1)+7),(yp1*((y)-ymax1)+7) 
#define KOORD_2(x,y) (xp2*((x)-xmin2)),(yp2*((y)-ymax2))   
#define M_PI 3.1415926535897932384626433832795



CModSystemZ1kapitzaDlg::CModSystemZ1kapitzaDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CModSystemZ1kapitzaDlg::IDD, pParent)
	, v(1)
	, A(1)
	, w0(0.1)
	, g(9.7)
	, l(4)
	, m(1)
	, dt(0.1)
	, timer_ptr(0)
	, r(0)
	, FTri(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	
}

void CModSystemZ1kapitzaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, v);
	DDX_Text(pDX, IDC_EDIT2, A);
	DDX_Text(pDX, IDC_EDIT3, w0);
	DDX_Text(pDX, IDC_EDIT4, g);
	DDX_Text(pDX, IDC_EDIT5, l);
	DDX_Text(pDX, IDC_EDIT6, m);
	DDX_Text(pDX, IDC_EDIT7, dt);
	DDX_Text(pDX, IDC_EDIT8, r);
	DDX_Text(pDX, IDC_EDIT9, FTri);
}

BEGIN_MESSAGE_MAP(CModSystemZ1kapitzaDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CModSystemZ1kapitzaDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CModSystemZ1kapitzaDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &CModSystemZ1kapitzaDlg::OnBnClickedButton1)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDOK2, &CModSystemZ1kapitzaDlg::OnBnClickedOk2)

END_MESSAGE_MAP()


// CModSystemZ1kapitzaDlg message handlers

BOOL CModSystemZ1kapitzaDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	SetIcon(m_hIcon, TRUE);			
	SetIcon(m_hIcon, FALSE);		

	PicWndMult = GetDlgItem(IDC_MODEL);
	PicDcMult = PicWndMult->GetDC();
	PicWndMult->GetClientRect(&PicMult);

	PicWndPhaza = GetDlgItem(IDC_PHAZA);
	PicDcPhaza = PicWndPhaza->GetDC();
	PicWndPhaza->GetClientRect(&PicPhaza);

	setka_pen.CreatePen(  //��� �����
		PS_DASH,    //�������� �����
		//PS_DOT,     //����������
		1,      //������� 1 �������
		RGB(0, 0, 0));  //���� �����

	osi_pen.CreatePen(   //������������ ���
		PS_SOLID,    //�������� �����
		3,      //������� 3 �������
		RGB(138, 138, 138));   //���� ������

	graf_pen.CreatePen(   //������
		PS_SOLID,    //�������� �����
		12,      //������� 2 �������
		RGB(0, 0, 0));   //���� ������

	graf_pen1.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(0, 0, 0));   //���� �������

	graf_penred.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(255, 0, 0));   //���� ������

	graf_pengreen.CreatePen(   //������
		PS_SOLID,    //�������� �����
		4,      //������� 2 �������
		RGB(0, 150, 0));   //���� ������

	graf_penblue.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(0, 0, 255));   //���� ������
	xmin1 = -5;			//����������� �������� �1
	xmax1 = 5;			//������������ �������� �1
	ymin1 = -5;			//����������� �������� y1
	ymax1 = 5;			//������������ �������� y1

	xmin2 = -1;			//����������� �������� �2
	xmax2 = 1;			//������������ �������� �2
	ymin2 = -1;			//����������� �������� y2
	ymax2 = 1;

	X = 0;//��������� ���������
	Y = 0;
	Phi = w0 * 2 * M_PI;
	PhiV = 0;
	time = 0;
	Ypodves = 3;
	RollsPlace(X, Y, Phi, Ypodves);
	PhazaList.clear();
	return TRUE; 
}



void CModSystemZ1kapitzaDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); 

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		///////////////////////////////////////////////
		///////////////////////////////////////////////
		xp1 = ((double)(PicMult.Width()) / (xmax1 - xmin1));			// ������������ ��������� ��������� �� x1
		yp1 = -((double)(PicMult.Height()) / (ymax1 - ymin1));       // ������������ ��������� ��������� �� y1
		xp2 = ((double)(PicPhaza.Width()) / (xmax2 - xmin2));			// ������������ ��������� ��������� �� x2
		yp2 = -((double)(PicPhaza.Height()) / (ymax2 - ymin2));          // ������������ ��������� ��������� �� y2
		///////////////////////////////////////////////
		///////////////////////////////////////////////
		// ��� MULT
		PicDcMult->FillSolidRect(&PicMult, RGB(200, 200, 200));    // ����������� ���
		PicDcMult->SelectObject(&osi_pen);		// �������� �����

		//������ ��� Y
		PicDcMult->MoveTo(KOORD(0, ymin1));
		PicDcMult->LineTo(KOORD(0, ymax1));
		//������ ��� �
		PicDcMult->MoveTo(KOORD(xmin1, 0));
		PicDcMult->LineTo(KOORD(xmax1, 0));

		//������� ����
		PicDcMult->TextOutW(KOORD(0.05*ymax1, ymax1), _T("Y"));   //Y
		PicDcMult->TextOutW(KOORD(xmax1 - 0.05*ymax1, -0.05*ymax1), _T("X"));     //X

		// ������ ������
		PicDcMult->SelectObject(&graf_pen);
		PicDcMult->Ellipse(KOORD_1_1(X, Y), KOORD_1_2(X, Y));
		PicDcMult->Ellipse(KOORD_1_1(0, Ypodves), KOORD_1_2(0, Ypodves));
		PicDcMult->SelectObject(&graf_pen1);
		PicDcMult->MoveTo(KOORD(0, Ypodves));
		PicDcMult->LineTo(KOORD(X, Y));
		///////////////////////////////////////////////
		///////////////////////////////////////////////
		// ��� PHAZA
		PicDcPhaza->FillSolidRect(&PicPhaza, RGB(200, 200, 200));    // ����������� ���
		PicDcPhaza->SelectObject(&osi_pen);		// �������� �����

		//������ ��� Y
		PicDcPhaza->MoveTo(KOORD_2(0, ymin2));
		PicDcPhaza->LineTo(KOORD_2(0, ymax2));
		//������ ��� �
		PicDcPhaza->MoveTo(KOORD_2(xmin2, 0));
		PicDcPhaza->LineTo(KOORD_2(xmax2, 0));

		//������� ����
		PicDcPhaza->TextOutW(KOORD_2(0.05*ymax2, ymax2), _T("fi`"));
		PicDcPhaza->TextOutW(KOORD_2(xmax2 - 0.05*ymax2, -0.05*ymax2), _T("fi"));
		
		
		
		//������� ����������
		if (PhazaList.size() != NULL)
		{
			PicDcPhaza->SelectObject(&graf_penblue);
			PicDcPhaza->MoveTo(KOORD_2(PhazaList[0].x, PhazaList[0].y));
			for (int i = 0; i < PhazaList.size(); i++)
			{
				PicDcPhaza->LineTo(KOORD_2(PhazaList[i].x, PhazaList[i].y));
			}
		}
		///////////////////////////////////////////////
		///////////////////////////////////////////////


	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CModSystemZ1kapitzaDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

double CModSystemZ1kapitzaDlg::f(double t, double phi, double phiv)     // (Phi)''
{
	
	double Buffer = -(A*v*v*cos(v*t) + g)*((sin(phi)) / l) - 2 * r / (m*m)*phiv;
	if (phiv < 0)
	{
		Buffer += FTri;
	}
	else
	{
		Buffer -= FTri;
	}	
	return Buffer;
}
void CModSystemZ1kapitzaDlg::RollsPlace(double &X, double& Y, double Phi, double &Y2)
{
	Y2 = 3 - A*sin(v*time);
	X = l*sin(Phi);
	Y = Y2 - l*cos(Phi);
}
double CModSystemZ1kapitzaDlg::Runge_Kutt(double &F, double &FV, double  &T)   // ����� �����-�����
{
	double x1_0 = F;
	double V1_0 = FV;
	double dtt = dt;
	double k1_x1 = f(T,x1_0,V1_0)*dtt;
	double k2_x1 = f(T, x1_0 + V1_0 * dtt / 2, V1_0 + k1_x1 / 2)*dtt;
	double k3_x1 = f(T, x1_0 + V1_0 * dtt / 2 + k1_x1*dtt / 4, V1_0 + k2_x1 / 2)*dtt;
	double k4_x1 = f(T, x1_0 + V1_0 * dtt + k2_x1*dtt / 2, V1_0 + k3_x1)*dtt;
	F = x1_0 + V1_0 * dtt + 1. / 6 * (k1_x1 + k2_x1 + k3_x1)*dtt;
	FV = V1_0 + 1. / 6 * (2 * k2_x1 + 2 * k3_x1 + k1_x1 + k4_x1);
	T += dtt;
	return 0;
}

void CModSystemZ1kapitzaDlg::OnTimer(UINT_PTR nIDEvent)
{
	Runge_Kutt(Phi,PhiV,time);
	RollsPlace( X,  Y, Phi, Ypodves);

	if (Y > ymax1)ymax1 = Y;
	ymin1 = -ymax1;
	xmin1 = ymin1; xmax1 = ymax1;

	Dot Buf; Buf.x = Phi; Buf.y = PhiV;
	if (abs(Phi) > ymax2)ymax2 = abs(Phi);
	if (abs(PhiV) > ymax2)ymax2 = abs(PhiV);
	xmin2 = ymin2 = -ymax2;
	xmax2 = ymax2;
	PhazaList.push_back(Buf);
	OnPaint();
}
void CModSystemZ1kapitzaDlg::OnBnClickedOk() // ����
{
	UpdateData(TRUE);
	PhazaList.clear();
	X = 0;//��������� ���������
	Y = 0;
	Phi = w0 * 2 * M_PI;
	PhiV = 0;
	Ypodves = 3;
	time = 0;
	UpdateData(FALSE);

	ymin2 = ymax2 = xmin2 = xmax2 = 0;
	ymax1 = 3 + A; xmax1 = ymax1;
	ymin1 = xmin1 = -ymax1;
//	OnBnClickedButton1(); //stop
	timer_ptr = SetTimer(10, 50, NULL);   // 10 - �������������, 50 - ��, NULL - ��� ������������� OnTimer()

}

void CModSystemZ1kapitzaDlg::OnBnClickedButton1() //����
{
	if (timer_ptr != NULL) KillTimer(timer_ptr);
}

void CModSystemZ1kapitzaDlg::OnBnClickedCancel() //�����
{
	
	CDialogEx::OnCancel();
}





	void CModSystemZ1kapitzaDlg::OnBnClickedOk2() //��������
	{
		UpdateData(TRUE);
		PhazaList.clear();
		X = 0;//��������� ���������
		Y = 0;
		Phi = w0 * 2 * M_PI;
		PhiV = 0;
		time = 0;
		Ypodves = 3;
		RollsPlace(X, Y, Phi, Ypodves);
		UpdateData(FALSE);
		if (Ypodves > ymax1)ymax1 = Ypodves;
		if (Ypodves < ymin1)ymin1 = Ypodves;
		if (Y > ymax1)ymax1 = Y;
		ymin1 = -ymax1;
		xmin1 = ymin1; xmax1 = ymax1;

		Invalidate();
	}


	
