
// ModSystemZ1(kapitza).h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CModSystemZ1kapitzaApp:
// See ModSystemZ1(kapitza).cpp for the implementation of this class
//

class CModSystemZ1kapitzaApp : public CWinApp
{
public:
	CModSystemZ1kapitzaApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CModSystemZ1kapitzaApp theApp;