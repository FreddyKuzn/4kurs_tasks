#pragma once
#include <list>     // �������� ���� ��� ������������� �������
#include "afxwin.h"
#include <vector>
using namespace std;



// CModSystemZ1kapitzaDlg dialog
class CModSystemZ1kapitzaDlg : public CDialogEx
{
	// Construction
public:
	CModSystemZ1kapitzaDlg(CWnd* pParent = NULL);	// standard constructor

	// Dialog Data
	enum { IDD = IDD_MODSYSTEMZ1KAPITZA_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


	// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButton1();
	///////////////////////////////////////////////
	///////////////////////////////////////////////
	//���������� ���������
	// ������� ������ CWnd
	CWnd * PicWndMult;
	CWnd * PicWndPhaza;
	// ������� ������ CDC
	CDC * PicDcMult;
	CDC * PicDcPhaza;
	// ������� ������ CRect
	CRect PicMult;
	CRect PicPhaza;
	//���������� ��� ������ � ���������
	double xp1, yp1,        // ����������� ���������
		xmin1, xmax1,	    // �������������� � ����������� �������� �1 
		ymin1, ymax1;       // �������������� � ����������� �������� y1

	double xp2, yp2,		// ����������� ���������
		xmin2, xmax2,		// �������������� � ����������� �������� �2
		ymin2, ymax2;       // �������������� � ����������� �������� y2  
	//�����
	CPen osi_pen;
	CPen setka_pen;
	CPen graf_pen;
	CPen graf_pen1;
	CPen graf_penred;
	CPen graf_pengreen;
	CPen graf_penblue;
	///////////////////////////////////////////////
	///////////////////////////////////////////////
	//����������
	double X, Y, Phi, PhiV; // ��� ������
	double time;

	double Ypodves; // ��� �������
	// ������� ����������� ������������ ������������� ��������� �������
	double v;
	//  ��������� ����������� ���������
	double A;
	// ����������� ������� ��������� ��������������� ��������
	double w0;
	// ��������� ���������� �������
	double g;
	//  ����� ������ �������
	double l;
	// ����� �������
	double m;
	//  ��� �� �������
	double dt;
	// ���-��� ������
	double r;
	// ������ ��� �������� �������
	struct Dot
	{
		double x, y;
	};
	vector <Dot> PhazaList;

	///////////////////////////////////////////////
	///////////////////////////////////////////////
	//�������
	double Runge_Kutt(double &F, double &FV, double  &T);   // ����� �����_�����
	double f(double t, double phi, double phiv);        // ������ ����� ��������� ��������
	void RollsPlace(double &X, double &Y, double Phi, double &Y2); // ������� ������� ����� ��������� ������� � �����
	//������
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	UINT_PTR timer_ptr;      // ������ �������
	afx_msg void OnBnClickedOk2();
	double FTri;
};
