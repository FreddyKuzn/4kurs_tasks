﻿
// FDTDDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "FDTD.h"
#include "FDTDDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CFDTDDlg



CFDTDDlg::CFDTDDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_FDTD_DIALOG, pParent)
	, Pole_count(200)
	, PML_count(16)
	, koeffA(10)
	, koeffB(10)
	, tau(0.1)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFDTDDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FTDTPIC, GDIobj); //ОТРИСОВКА GDI+
	DDX_Control(pDX, IDC_SLIDER1, Pic_slider);
	DDX_Text(pDX, IDC_EDIT1, Pole_count);
	DDX_Text(pDX, IDC_EDIT2, PML_count);
	DDV_MinMaxInt(pDX, PML_count, 4, 16);
	DDX_Text(pDX, IDC_EDIT3, koeffA);
	DDX_Text(pDX, IDC_EDIT4, koeffB);
	DDX_Text(pDX, IDC_EDIT5, tau);
}

BEGIN_MESSAGE_MAP(CFDTDDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_HSCROLL()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDOK, &CFDTDDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CFDTDDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// Обработчики сообщений CFDTDDlg

BOOL CFDTDDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию
	Pic_slider.SetRangeMin(1);
	Pic_slider.SetRangeMax(4);
	Pic_slider.SetPos(1);
	SliderMode = 1;
	timer_work = 0;
	GDIobj.Invalidate();

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CFDTDDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);

	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CFDTDDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CFDTDDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar * pScrollBar)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного
	SliderMode = Pic_slider.GetPos();
	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}


void CFDTDDlg::OnBnClickedOk()
{
	if (!timer_work)
	{
		UpdateData(1);

		int full_count = Pole_count + 2*PML_count + 2;// +2 - по одному ряду бездны на границе
		Pole.clear();
		Pole.resize(full_count);
		for (int i = 0; i < full_count; i++) Pole[i].resize(full_count);
		PolePast.clear();
		PolePast.resize(full_count);
		for (int i = 0; i < full_count; i++) PolePast[i].resize(full_count);
		//Выделение PML границ
		double sigmaElMAX = 100;
		for (int i = 0; i < PML_count+1; i++)
		{
			for (int j = i; j < full_count-i; j++)
			{
				Pole[i][j].PML = 1;
				Pole[i][j].sigmaElx = sigmaElMAX * ((double)i / (double)PML_count);
				Pole[i][j].sigmaEly = sigmaElMAX * ((double)i / (double)PML_count);

				Pole[full_count - i-1][j].PML = 1;
				Pole[full_count - i-1][j].sigmaElx = sigmaElMAX * ((double)i / (double)PML_count);
				Pole[full_count - i-1][j].sigmaEly = sigmaElMAX * ((double)i / (double)PML_count);

				Pole[j][i].PML = 1;
				Pole[j][i].sigmaElx = sigmaElMAX * ((double)i / (double)PML_count);
				Pole[j][i].sigmaEly = sigmaElMAX * ((double)i / (double)PML_count);

				Pole[j][full_count - i - 1].PML = 1;
				Pole[j][full_count - i - 1].sigmaElx = sigmaElMAX * ((double)i / (double)PML_count);
				Pole[j][full_count - i - 1].sigmaEly = sigmaElMAX * ((double)i / (double)PML_count);
			}
		}
		//Выделение источника
		Pole[full_count / 2][full_count / 2].source = 1;
		//Выделение границы бездны
		for (int i = 0; i < full_count; i++)
		{
			Pole[i][0].abyss = 1;
			Pole[i][full_count - 1].abyss = 1;
			Pole[0][i].abyss = 1;
			Pole[full_count - 1][i].abyss = 1;
		}
		ExMax=ExMin=EyMax=EyMin=HzMax=HzMin=ModMax=ModMin = 0;
		full_tau = 0;
		SetTimer(1, 50, NULL);
		timer_work = 1;
		GetDlgItem(IDOK)->SetWindowText(L"Stop");
	}
	else
	{
		KillTimer(1); timer_work = 0;
		GetDlgItem(IDOK)->SetWindowText(L"Play");
	}
}


void CFDTDDlg::OnBnClickedCancel()
{
	CDialogEx::OnCancel();
}

void CFDTDDlg::LetsGo()
{
	for (int i = 0; i < Pole.size(); i++)
	{
		for (int j = 0; j < Pole[i].size(); j++)
		{
			if (!Pole[i][j].abyss)
			{
				if (Pole[i][j].PML)
				{
					ExNewPML(i, j);
					EyNewPML(i, j);
					HzxNew(i, j);
					HzyNew(i, j);
					Pole[i][j].MOD = sqrt(pow(Pole[i][j].Hzx, 2) + pow(Pole[i][j].Hzy, 2) + pow(Pole[i][j].Ex, 2) + pow(Pole[i][j].Ey, 2));

					MINMAX(Pole[i][j].Ex, ExMin, ExMax);
					MINMAX(Pole[i][j].Ey, EyMin, EyMax);
					MINMAX(sqrt(pow(Pole[i][j].Hzx,2)+ pow(Pole[i][j].Hzy, 2)), HzMin, HzMax);
					MINMAX(Pole[i][j].MOD, ModMin, ModMax);
				}
				else
				{
					ExNew(i, j);
					EyNew(i, j);
					if (Pole[i][j].source)
					{
						Pole[i][j].Ex += koeffA*cos(full_tau);
						Pole[i][j].Ey += koeffB*sin(full_tau);
					}
					HzNew(i, j);
					Pole[i][j].MOD = sqrt(pow(Pole[i][j].Hz, 2)+ pow(Pole[i][j].Ex, 2) + pow(Pole[i][j].Ey, 2));

					MINMAX(Pole[i][j].Ex, ExMin, ExMax);
					MINMAX(Pole[i][j].Ey, EyMin, EyMax);
					MINMAX(Pole[i][j].Hz, HzMin, HzMax);
					MINMAX(Pole[i][j].MOD, ModMin, ModMax);
				}
			}
			PolePast[i][j] = Pole[i][j];
		}
	}
}

void CFDTDDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного
	LetsGo();
	full_tau += tau;
	GDIobj.massPic.clear();
	GDIobj.massPic.resize(Pole.size());
	for (int i = 0; i < Pole.size(); i++)
	{
		GDIobj.massPic[i].resize(Pole.size());
		for (int j = 0; j < Pole.size(); j++)
		{
			switch (SliderMode)
			{
			case 1:				
				GDIobj.GradientCell(GDIobj.massPic[i][j], Pole[i][j].Ex, ExMin, ExMax);
				break;
			case 2:
				GDIobj.GradientCell(GDIobj.massPic[i][j], Pole[i][j].Ey, EyMin, EyMax);
				break;
			case 3:				
				GDIobj.GradientCell(GDIobj.massPic[i][j], Pole[i][j].Hz, HzMin, HzMax);
				break;
			case 4:
				GDIobj.GradientCell(GDIobj.massPic[i][j], Pole[i][j].MOD, ModMin, ModMax);
				break;
			default:
				break;
			}
		}
	}
	GDIobj.MyPaint();
	CDialogEx::OnTimer(nIDEvent);
}