﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется FDTD.rc
//
#define IDD_FDTD_DIALOG                 102
#define IDR_MAINFRAME                   128
#define IDC_FTDTPIC                     1000
#define IDC_SLIDER1                     1004
#define IDC_EDIT1                       1005
#define IDC_EDIT2                       1007
#define IDC_EDIT3                       1008
#define IDC_EDIT4                       1010
#define IDC_EDIT5                       1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
