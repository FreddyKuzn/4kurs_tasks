#pragma once
#include <afxwin.h>
#include <math.h>
#include <vector>
using namespace std;
#define M_PI 3.1415926535
class GDIClass :
	public CStatic
{
	//////////////////////////////////////////
	DECLARE_DYNAMIC(GDIClass)
public:
	GDIClass();
	virtual ~GDIClass();
	virtual void DrawItem(LPDRAWITEMSTRUCT);
private:
protected:
	DECLARE_MESSAGE_MAP()
public:
	int xmax = 100;
	int xmin = 0;
	int ymax = 100;
	int ymin = 0;

	struct GDIDot //�����
	{
		//Gdiplus::REAL x = 0.;
		//Gdiplus::REAL y = 0.; //����������
		BYTE R=0;
		BYTE G=0;
		BYTE B=0;
	};
	std::vector <vector<GDIDot>> massPic;
	void MyPaint()
	{
		//��������� � ���������
		if (massPic.size() != 0)
		{
			xmax = massPic.size();
			ymax = massPic.size();
		}
		Invalidate();
	}
	void GradientCell(GDIDot &MyDot,double value, double GradMin, double GradMax)
	{
		if (massPic.size() != 0)
		{
			double GradRange = GradMax - GradMin;
			int buf = (int)(((value- GradMin) / GradRange)*510);
			if (buf > 255)
			{
				buf -= 255;
				MyDot.R = (BYTE)buf;
				MyDot.G = 0;
				MyDot.B = 0;
			}
			else
			{				
				MyDot.R = 0;
				MyDot.G = 0;
				MyDot.B = 255 - (BYTE)buf;
			}
		}
	}
	//////////////////////////////////////////


};

