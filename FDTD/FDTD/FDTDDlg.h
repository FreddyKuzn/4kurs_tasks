﻿
// FDTDDlg.h: файл заголовка
//

#pragma once
#include "GDIClass.h"
#include <gl/GL.h>
using namespace std;
// Диалоговое окно CFDTDDlg
class CFDTDDlg : public CDialogEx
{
// Создание
public:
	CFDTDDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_FDTD_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CSliderCtrl Pic_slider;
	int SliderMode;
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	bool timer_work;
	GDIClass GDIobj; //GDICLASS
	//==============================================
	struct DotPole
	{
		bool PML = 0; //точка является pml границей
		bool abyss = 0;
		double sigmaElx = 0;//
		double sigmaEly = 0;//
		double sigmaMx = 0;//
		double sigmaMy = 0;//
		bool source=0; //точка - источник
		double Ex = 0;
		double Ey = 0;
		double Hz = 0;
		double Hzx = 0;
		double Hzy = 0;
		double MOD = 0;
	};
	double full_tau;
	double tau;//шаг по времени
	double e=1;
	double nu=1;
	double Pole_step=1;//расстояние между узлами
	vector<vector<DotPole>> Pole; //на n шаге
	vector<vector<DotPole>> PolePast;//на n-1 шаге
	int Pole_count;
	void LetsGo();
	/////////////////////////////////////
	///Функции расчёта новых значений////
	/////////////////////////////////////
	void ExNew(int i, int j)
	{
		Pole[i][j].Ex = PolePast[i][j].Ex + (2 * tau / e) * ((Pole[i][j + 1].Hz - Pole[i][j - 1].Hz) / (2 * Pole_step));
	}
	void EyNew(int i, int j)
	{
		Pole[i][j].Ey = PolePast[i][j].Ey - (2 * tau / e) * ((Pole[i+1][j].Hz - Pole[i-1][j].Hz) / (2 * Pole_step));
	}
	void HzNew(int i, int j)
	{
		Pole[i][j].Hz = PolePast[i][j].Hz - (tau / nu) * (((Pole[i + 1][j].Ey - Pole[i - 1][j].Ey) / (2 * Pole_step)) - ((Pole[i][j+1].Ex - Pole[i][j-1].Ex) / (2 * Pole_step)));
	}
	void HzxNew(int i, int j)
	{
		Pole[i][j].Hzx = -(2 * tau / (2 * nu + tau * Pole[i][j].sigmaMx)) * ((Pole[i + 1][j].Ey - Pole[i - 1][j].Ey) / (2 * Pole_step))
			- PolePast[i][j].Hzx * ((2*nu)-(tau* Pole[i][j].sigmaMx)) / ((2 * nu)+ (tau * Pole[i][j].sigmaMx));
	}
	void HzyNew(int i, int j)
	{
		Pole[i][j].Hzy = -(2 * tau / (2 * nu + tau * Pole[i][j].sigmaMy)) * ((Pole[i][j+1].Ex - Pole[i][j-1].Ex) / (2 * Pole_step))
			- PolePast[i][j].Hzy * ((2 * nu) - (tau * Pole[i][j].sigmaMy)) / ((2 * nu) + (tau * Pole[i][j].sigmaMy));
	}
	double AAAy(int i, int j)
	{
		return ((PolePast[i][j + 1].Hzy - PolePast[i][j - 1].Hzy) / (2 * Pole_step) + (PolePast[i][j + 1].Hzx - PolePast[i][j - 1].Hzx) / (2 * Pole_step));
	}
	double AAAx(int i, int j)
	{
		return ((PolePast[i+1][j].Hzy - PolePast[i-1][j].Hzy) / (2 * Pole_step) + (PolePast[i+1][j].Hzx - PolePast[i-1][j].Hzx) / (2 * Pole_step));
	}
	void ExNewPML(int i, int j)
	{
		Pole[i][j].Ex = PolePast[i][j].Ex + (2 * tau / (2 * e + tau * Pole[i][j].sigmaEly)) * AAAy(i,j);
	}
	void EyNewPML(int i, int j)
	{
		Pole[i][j].Ey = PolePast[i][j].Ey + (2 * tau / (2 * e + tau * Pole[i][j].sigmaElx)) * AAAx(i, j);
	}
	double ExMax, ExMin, EyMax, EyMin, HzMax, HzMin, ModMax, ModMin;
	void MINMAX(double value, double &Min, double &Max)
	{
		if (value < Min)Min = value;
		if (value > Max)Max = value;
	}
	int PML_count;
	double koeffA;
	double koeffB;
};
