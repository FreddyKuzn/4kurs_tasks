
// ShredingerDlg.h: файл заголовка
//

#pragma once
#include <vector>
#include <cmath>
using namespace std;
// Диалоговое окно CShredingerDlg
class CShredingerDlg : public CDialogEx
{
// Создание
public:
	CShredingerDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SHREDINGER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	//Переменные рисования
// объекты класса CWnd
	CWnd * PicWndMult;
	// объекты класса CDC
	CDC * PicDcMult;
	// объекты класса CRect
	CRect PicMult;
	//Переменные для работы с масштабом
	double xp1, yp1,        
		xmin1, xmax1,	    
		ymin1, ymax1;       

	CWnd * PicWndMult2;
	// объекты класса CDC
	CDC * PicDcMult2;
	// объекты класса CRect
	CRect PicMult2;

	double xp2, yp2,        
		xmin2, xmax2,	    
		ymin2, ymax2;       
		//ручки
	CPen osi_pen;
	CPen setka_pen;
	CPen graf_pen;
	CPen graf_pen1;
	CPen graf_penred;
	CPen graf_pengreen;
	CPen graf_penblue;
	///////////////////////////////////////////////
	double R; //бесконечность не предел
	vector <double> u; //распределение потенциала
	vector <double> fi; //Текущее значение распределения фазовой функции
	vector <double> fi_past;//предыдущее распределение фазовой функции
	vector <double> Fre;
	vector <double> MasEnergy;
	vector <double> r;
	vector <double> Wave_function;
	double MediumEnergy(double Min, double Max, double FreK);
	int dotscount; //количество точек
	int dotscountEnergy;
	double step; //шаг по длине 
	double BufE; //значение энергии для count_difference
	double error;
	double V0;// высота барьера
	void UPot(int kolvo, double V0); //функция распределения потенциала;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	double Emin;
	double Emax;
	int kolvoHoles;
	int Kmax;
	afx_msg void OnBnClickedOk2();
	int k_value;
	bool WaveType;
};
