
// DynamicsWaveProcessDlg.cpp: файл реализации
//

#include "stdafx.h"
#include "DynamicsWaveProcess.h"
#include "DynamicsWaveProcessDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CDynamicsWaveProcessDlg
#define KOORD1(x,y) (xp1*((x)-xmin1)),(yp1*((y)-ymax1)) 
#define KOORD2(x,y) (xp2*((x)-xmin2)),(yp2*((y)-ymax2))
#define KOORD3(x,y) (xp3*((x)-xmin3)),(yp3*((y)-ymax3))
#define M_PI 3.1415926535


CDynamicsWaveProcessDlg::CDynamicsWaveProcessDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DYNAMICSWAVEPROCESS_DIALOG, pParent)
	, R(2)
	, dotscountR(1000)
	, TimeStep(0.01)
	, dotscountTime(512)
	, V0(1)
	, EvolProgress(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDynamicsWaveProcessDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDER1, Slider1);
	DDX_Control(pDX, IDC_SLIDER2, Slider2);
	DDX_Text(pDX, IDC_EDIT1, R);
	DDX_Text(pDX, IDC_EDIT2, dotscountR);
	DDX_Text(pDX, IDC_EDIT3, TimeStep);
	DDX_Text(pDX, IDC_EDIT4, dotscountTime);
	DDX_Text(pDX, IDC_EDIT5, V0);
	DDX_Text(pDX, IDC_EDIT6, EvolProgress);
}

BEGIN_MESSAGE_MAP(CDynamicsWaveProcessDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CDynamicsWaveProcessDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CDynamicsWaveProcessDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON2, &CDynamicsWaveProcessDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON1, &CDynamicsWaveProcessDlg::OnBnClickedButton1)
	ON_WM_HSCROLL()
END_MESSAGE_MAP()


// Обработчики сообщений CDynamicsWaveProcessDlg

BOOL CDynamicsWaveProcessDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок


	PicWndMult1 = GetDlgItem(IDC_STATIC1);
	PicDcMult1 = PicWndMult1->GetDC();
	PicWndMult1->GetClientRect(&PicMult1);

	PicWndMult2 = GetDlgItem(IDC_STATIC2);
	PicDcMult2 = PicWndMult2->GetDC();
	PicWndMult2->GetClientRect(&PicMult2);

	PicWndMult3 = GetDlgItem(IDC_STATIC3);
	PicDcMult3 = PicWndMult3->GetDC();
	PicWndMult3->GetClientRect(&PicMult3);

	setka_pen.CreatePen(  //для сетки
		PS_DASH,    //сплошная линия
		//PS_DOT,     //пунктирная
		1,      //толщина 1 пиксель
		RGB(155, 155, 155));  //цвет серый

	osi_pen.CreatePen(   //координатные оси
		PS_SOLID,    //сплошная линия
		3,      //толщина 3 пикселя
		RGB(0, 0, 0));   //цвет черный

	graf_pen.CreatePen(   //график
		PS_DASH,    //сплошная линия
		1,      //толщина 2 пикселя
		RGB(0, 0, 0));   //цвет черный

	graf_pen1.CreatePen(   //график
		PS_SOLID,    //сплошная линия
		2,      //толщина 2 пикселя
		RGB(255, 0, 0));   //цвет розовый

	graf_penred.CreatePen(   //график
		PS_SOLID,    //сплошная линия
		2,      //толщина 2 пикселя
		RGB(255, 0, 0));   //цвет черный

	graf_pengreen.CreatePen(   //график
		PS_DASH,    //сплошная линия
		2,      //толщина 2 пикселя
		RGB(0, 255, 0));   //цвет черный

	graf_penblue.CreatePen(   //график
		PS_SOLID,    //сплошная линия
		1,      //толщина 2 пикселя
		RGB(0, 0, 255));   //цвет черный




	OnBnClickedOk();

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}
void CDynamicsWaveProcessDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		///////////////////////////////////////////////
		///////////////////////////////////////////////
		xp1 = ((double)(PicMult1.Width()) / (xmax1 - xmin1));			// Коэффициенты пересчёта координат по x1
		yp1 = -((double)(PicMult1.Height()) / (ymax1 - ymin1));       // Коэффициенты пересчёта координат по y1

		xp2 = ((double)(PicMult2.Width()) / (xmax2 - xmin2));			// Коэффициенты пересчёта координат по x2
		yp2 = -((double)(PicMult2.Height()) / (ymax2 - ymin2));       // Коэффициенты пересчёта координат по y2

		xp3 = ((double)(PicMult3.Width()) / (xmax3 - xmin3));			// Коэффициенты пересчёта координат по x3
		yp3 = -((double)(PicMult3.Height()) / (ymax3 - ymin3));       // Коэффициенты пересчёта координат по y3
		///////////////////////////////////////////////
		///////////////////////////////////////////////
		// для MULT1
		PicDcMult1->FillSolidRect(&PicMult1, RGB(255, 255, 255));    // закрашиваем фон
		PicDcMult1->SelectObject(&osi_pen);		// выбираем ручку

		//создаём Ось Y
		PicDcMult1->MoveTo(KOORD1(0, ymin1));
		PicDcMult1->LineTo(KOORD1(0, ymax1));
		//создаём Ось Х
		PicDcMult1->MoveTo(KOORD1(xmin1, 0));
		PicDcMult1->LineTo(KOORD1(xmax1, 0));

		if (WavePImg.size() != 0)
		{
			PicDcMult1->SelectObject(&graf_pen);		// выбираем ручку
			PicDcMult1->MoveTo(KOORD1(0, u[0]));
			for (int i = 0; i < u.size(); i++)
			{
				PicDcMult1->LineTo(KOORD1(i, u[i]));
			}
			PicDcMult1->SelectObject(&graf_pen1);		// выбираем ручку
			PicDcMult1->MoveTo(KOORD1(0, WavePImg[0].real()));
			for (int i = 0; i < WavePImg.size(); i++)
			{
				PicDcMult1->LineTo(KOORD1(i, WavePImg[i].real()));
			}

		}
		if (EvolProgress == dotscountTime - 1)
		{
			UpdateData(1);
			EvolProgress++;
			UpdateData(0);
			KillTimer(timer_ptr);

		}

		///////////////////////////////////////////////
		// для MULT2
		PicDcMult2->FillSolidRect(&PicMult2, RGB(255, 255, 255));    // закрашиваем фон
		PicDcMult2->SelectObject(&osi_pen);		// выбираем ручку

		//создаём Ось Y
		PicDcMult2->MoveTo(KOORD2(0, ymin2));
		PicDcMult2->LineTo(KOORD2(0, ymax2));
		//создаём Ось Х
		PicDcMult2->MoveTo(KOORD2(xmin2, 0));
		PicDcMult2->LineTo(KOORD2(xmax2, 0));

		if (Furie.size() != 0)
		{
			PicDcMult2->SelectObject(&graf_pen1);		// выбираем ручку
			PicDcMult2->MoveTo(KOORD2(0, sqrt(Furie[0].real() * Furie[0].real())));
			for (int i = 0; i < Furie.size(); i++)
			{
				PicDcMult2->LineTo(KOORD2(i, sqrt(Furie[i].real() * Furie[i].real())));
			}
		}


		///////////////////////////////////////////////
		// для MULT3
		PicDcMult3->FillSolidRect(&PicMult3, RGB(255, 255, 255));    // закрашиваем фон
		PicDcMult3->SelectObject(&osi_pen);		// выбираем ручку

		//создаём Ось Y
		PicDcMult3->MoveTo(KOORD3(0, ymin3));
		PicDcMult3->LineTo(KOORD3(0, ymax3));
		//создаём Ось Х
		PicDcMult3->MoveTo(KOORD3(xmin3, 0));
		PicDcMult3->LineTo(KOORD3(xmax3, 0));

		//////////////////////////////////////////////
		//////////////////////////////////////////////
		PicDcMult1->SelectObject(&graf_pengreen);
		//создаём Ось Слайдера 1
		PicDcMult1->MoveTo(KOORD1(Slider1Value, ymin1));
		PicDcMult1->LineTo(KOORD1(Slider1Value, ymax1));

		PicDcMult2->SelectObject(&graf_pengreen);
		//создаём Ось Слайдера 2
		PicDcMult2->MoveTo(KOORD2(Slider2Value, ymin2));
		PicDcMult2->LineTo(KOORD2(Slider2Value, ymax2));


		if (WaveFunc.size() != 0)
		{
			PicDcMult3->SelectObject(&graf_pen1);		// выбираем ручку
			PicDcMult3->MoveTo(KOORD3(0, WaveFunc[0]));
			for (int i = 0; i < WaveFunc.size(); i++)
			{
				PicDcMult3->LineTo(KOORD3(i, WaveFunc[i]));
			}
		}
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CDynamicsWaveProcessDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CDynamicsWaveProcessDlg::OnBnClickedOk() //Reset
{
	UpdateData(1);
	if (timer_ptr != NULL) KillTimer(timer_ptr);
	EvolProgress = 0;
	xmax1 = dotscountR;			//Максимальное значение х1
	ymax1 = 5;			//Максимальное значение y1
	xmin1 = 0;			//Минимальное значение х1
	ymin1 = -0.1*ymax1;			//Минимальное значение y1

	xmax2 = dotscountTime;			//Максимальное значение х1
	ymax2 = 5;			//Максимальное значение y1
	xmin2 = 0;			//Минимальное значение х1
	ymin2 = -0.1*ymax2;			//Минимальное значение y1

	xmax3 = dotscountR;			//Максимальное значение х3
	ymax3 = 5;			//Максимальное значение y3
	xmin3 = 0;			//Минимальное значение х3
	ymin3 = -0.1*ymax3;			//Минимальное значение y3

	Slider1.SetRangeMin(0);
	Slider1.SetRangeMax(dotscountR - 1);
	Slider2.SetRangeMin(0);
	Slider2.SetRangeMax(dotscountTime - 1);
	on1 = false;
	WaveFunc.resize(0);
	fi.resize(0);
	r.resize(0);
	Furie.resize(0);
	SETKA.resize(0);
	SETKA_FUR.resize(0);
	Psixo.resize(0);
	u.resize(0);
	SETKA.resize(dotscountTime);
	SETKA_FUR.resize(dotscountTime);
	for (int i = 0; i < SETKA.size(); i++)
	{
		SETKA[i].resize(dotscountR);
		SETKA_FUR[i].resize(dotscountR);
	}
	Psixo.resize(dotscountR);
	u.resize(dotscountR);
	Step = 2 * R / dotscountR;
	UPot(5, V0);
	PsiPot();
	WavePImg = Psixo;
	SETKA[0] = Psixo;
	if (Max(WavePImg) > Max(u))ymax1 = Max(WavePImg); else ymax1 = Max(u);
	if (Min(WavePImg) < Min(u))ymin1 = Min(WavePImg); else ymin1 = Min(u);
	UpdateData(0);
	Invalidate();
}

void CDynamicsWaveProcessDlg::OnTimer(UINT_PTR nIDEvent)
{
	UpdateData(1);
	EvolProgress++;

	Complex A = AK(TimeStep, Step);
	Complex B = BK(TimeStep, Step);
	vector <Complex> C; C.resize(dotscountR);
	vector <Complex> D; D.resize(dotscountR);
	vector <Complex> a; a.resize(dotscountR);
	vector <Complex> b; b.resize(dotscountR);
	for (int i = 1; i < dotscountR - 1; i++)
	{
		C[i] = CK(TimeStep, Step, u[i]);
		D[i] = DK(TimeStep, Step, SETKA[EvolProgress - 1][i], delta2(Step, SETKA[EvolProgress - 1][i - 1], SETKA[EvolProgress - 1][i], SETKA[EvolProgress - 1][i + 1]), u[i]);
	}
	a[0] = 0;
	b[0] = 0;
	for (int i = 1; i < dotscountR; i++)
	{
		a[i] = ak(a[i - 1], A, B, C[i - 1]);
		b[i] = bk(a[i - 1], b[i - 1], A, C[i - 1], D[i - 1]);
	}
	SETKA[EvolProgress][dotscountR - 1] = 0;
	for (int i = dotscountR - 2; i >= 0; i--)
	{
		SETKA[EvolProgress][i] = a[i + 1] * SETKA[EvolProgress][i + 1] + b[i + 1];
	}
	WavePImg = SETKA[EvolProgress];
	UpdateData(0);
	InvalidateRect(&PicMult1);
}



void CDynamicsWaveProcessDlg::OnBnClickedButton1() //Эволюция
{
	OnBnClickedOk();
	timer_ptr = SetTimer(10, 50, NULL);
}

void CDynamicsWaveProcessDlg::OnBnClickedButton2() //Фурье
{
	for (int j = 0; j < dotscountR; j++)
	{
		Furie.resize(0);
		Furie.resize(dotscountTime);
		for (int i = 0; i < dotscountTime; i++)
		{
			Furie[i] = SETKA[i][j];
		}
		fur(Furie, 1);
		for (int i = 0; i < dotscountTime; i++)
		{
			SETKA_FUR[i][j] = Furie[i];
		}
	}


	on1 = true;


}

void CDynamicsWaveProcessDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	Slider1Value = Slider1.GetPos();
	Slider2Value = Slider2.GetPos();
	InvalidateRect(&PicMult1);

	if (on1)
	{

		for (int i = 0; i < dotscountTime; i++)
		{
			Furie[i] = SETKA_FUR[i][Slider1Value];
		}
		ymax2 = Max(Furie);
		ymin2 = Min(Furie);
		InvalidateRect(&PicMult2);

		WaveFunc.resize(dotscountR);
		for (int i = 0; i < dotscountR; i++)
		{
			WaveFunc[i] = SETKA_FUR[Slider2Value][i].real();
		}
		ymax3 = Max(WaveFunc);
		ymin3 = Min(WaveFunc);
		InvalidateRect(&PicMult3);
	}



	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}


void CDynamicsWaveProcessDlg::OnBnClickedCancel()
{
	CDialogEx::OnCancel();
}

void CDynamicsWaveProcessDlg::UPot(int kolvo, double V0)
{
	/*int x_dlina = (u.size()) / (2 * kolvo);
	int dist_pits = u.size() / (2 * (kolvo + 1));
	for (int i = 0; i < u.size(); i++)
	{
		u[i] = 0;
	}
	int buf = 0;
	for (int i = 0; i < kolvo; i++)
	{
		for (int j = dist_pits + buf; j < (buf + dist_pits + x_dlina); j++)
			u[j] = -V0;
		buf += dist_pits + x_dlina;
	}*/
	for (int i = 0; i < u.size(); i++)
	{		
			u[i] = -V0;		
	}
	int ser = (int)(u.size() / 2);
	int kkk = 10;
	for (int i = ser-kkk; i < ser+kkk; i++)
	{
		u[i] = 100000;
	}

}

void CDynamicsWaveProcessDlg::PsiPot()
{
	double Psi0 = 1.;
	double a = 0;
	double sigma = 0.1;
	for (int i = 0; i < dotscountR; i++)
	{
		double x = -R + Step * i;
		double E = exp(-(x - a)*(x - a) / (4 * sigma*sigma));
		Psixo[i] = Psi0 * E;
	}
}
double CDynamicsWaveProcessDlg::Max(vector<double> Mass)
{
	double ymax = 0;
	for (int j = 0; j < Mass.size(); j++)
	{
		if (Mass[j] > ymax)ymax = Mass[j];
	}
	return ymax;
}
double CDynamicsWaveProcessDlg::Max(vector<Complex> Mass)
{
	double ymax = 0;
	for (int j = 0; j < Mass.size(); j++)
	{
		if (Mass[j].real() > ymax)
			ymax = Mass[j].real();
		if (Mass[j].imag() > ymax)
			ymax = Mass[j].imag();
	}
	return ymax;
}
double CDynamicsWaveProcessDlg::Min(vector<double> Mass)
{
	double ymax = 0;
	for (int j = 0; j < Mass.size(); j++)
	{
		if (Mass[j] < ymax)ymax = Mass[j];
	}
	return ymax;
}
double CDynamicsWaveProcessDlg::Min(vector<Complex> Mass)
{
	double ymax = 0;
	for (int j = 0; j < Mass.size(); j++)
	{
		if (Mass[j].real() < ymax)ymax = Mass[j].real();
		if (Mass[j].imag() < ymax)ymax = Mass[j].imag();
	}
	return ymax;
}
