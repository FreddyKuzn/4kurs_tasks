/*process(1):-summar.
process(2):-maxEl.
process(3):-list.*/

/*����� ���� ����� � ������*/

summar:-
	nl,
	read([Head|Tail]),
	sum_list([Head|Tail], Sum),
	write('�����: '), write(Sum).
	
sum_list([], 0).
sum_list([Head|Tail], Sum):-
				sum_list(Tail, TailSum),
				Sum is TailSum + Head.

/*������������ ������� ������*/

maxEl:-
	nl,
	read([Head|Tail]),
	max_list([Head|Tail], MaxElem),
	write('������������ ��������: '), write(MaxElem).

max_list([MaxElem], MaxElem).
max_list([Head|Tail], MaxElem):-
				max_list(Tail, TailMaxElem),
				TailMaxElem > Head, 
				MaxElem = TailMaxElem;
				MaxElem = Head.

/*������ � ��������� ���������*/
/*list:-
	nl,
	read([Head1,Head2|Tail]),
	del_list([Head1,Head2|Tail],[Head1|NewTail]),
	write('���������: '), write([Head1|NewTail]).
 

del_list([],[]).
del_list([Head],[Head]).
del_list([Head1,Head2|Tail],[Head1|NewTail]):- 
				del_list(Tail, NewTail).*/



list:-
	nl,
	read([Head1,Head2|Tail]),
	del_list([Head1,Head2|Tail],[Head2|NewTail]),
	write([Head2|NewTail]).
 

del_list([],[]).
del_list([Head],[]).
del_list([Head1,Head2|Tail],[Head2|NewTail]):- 
				del_list(Tail, NewTail).
