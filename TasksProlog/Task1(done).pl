parent('Alex','Misha').
parent('Alex','Masha').
parent('Alex','Dima').
parent('Natasha','Misha').
parent('Natasha','Masha').
parent('Natasha','Dima').
parent('Dima','AlexJr').
parent('Olesa','AlexJr').
parent('Galina','Andrey').
parent('Pavel','Andrey').
parent('Lili','Kira').
parent('Andrey','Kira').
parent('Kate','Natasha').
parent('Kate','Galina').

man('Alex').
man('Misha').
man('Dima').
man('Pavel').
man('Andrey').
man('AlexJr').

woman('Natasha').
woman('Masha').
woman('Olesa').
woman('Galina').
woman('Lili').
woman('Kira').
woman('Kate').


married('Alex','Natasha').
married('Dima','Olesa').
married('Galina','Pavel').
married('Andrey','Lili').

check_married(X, Y):-
  married(X, Y); married(Y, X).

father(X,Y):-parent(X,Y), man(X).
mother(X,Y):-parent(X,Y), woman(X).

son(X,Y):-parent(Y,X), man(X).
daughter(X,Y):-parent(Y,X), woman(X).

siblings(X,Y):-parent(Z,X), parent(Z,Y), X\=Y.

aunt(X,Y):-parent(Z, Y), siblings(X,Z), woman(X).
uncle(X,Y):-parent(Z,Y), siblings(X,Z), man(X).

two_siblings(X,Y):-parent(Z,Y), aunt(Z,X); uncle(Z,X).

grandfather(X,Y):-parent(Z,Y), father(X,Z).
grandmother(X,Y):-parent(Z,Y), mother(X,Z).

wife(X,Y):-married(Y,X), woman(X).
husband(X,Y):-married(Y,X), man(X).
