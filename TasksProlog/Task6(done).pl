 /*������� 1*/

name('�������').
name('������').
name('����').
name('���').

work('������������').
work('��������').
work('�������').
work('����������').
work('�����������').
work('�������').
work('�����').
work('������').

rule(X,Y,Z):-
		name(X),
		X='�������',
		work(Y),
	 	work(Z),
		Y\=Z,
		not(Y='������'),
		not(Z='������'),
		not(Y='�������'),
		not(Z='�������'),
		not(Y='����������'),
		not(Z='����������'),
		not(Y='������������'),
		not(Z='������������'),
		not(Y='�����������'),
		not(Z='�����������'),
		not(Y='��������'),
		not(Z='��������').

rule(X,Y,Z):-
		name(X),
		X='������',
		work(Y),
	 	work(Z),
		Y\=Z,
		not(Y='�������'),
		not(Z='�������'),
		not(Y='����������'),
		not(Z='����������'),
		not(Y='�����������'),
		not(Z='�����������'),
		not(Y='�������'),
		not(Z='�������'),
		not(Y='�����'),
		not(Z='�����'),
		not(Y='��������'),
		not(Z='��������').

rule(X,Y,Z):-
		name(X),
		X='����',
		work(Y),
	 	work(Z),
		Y\=Z,
		not(Y='������������'),
		not(Z='������������'),
		not(Y='�������'),
		not(Z='�������'),
		not(Y='�������'),
		not(Z='�������'),
		not(Y='�����'),
		not(Z='�����'),
		not(Y='��������'),
		not(Z='��������'),
		not(Y='������'),
		not(Z='������').

rule(X,Y,Z):-
		name(X),
		X='���',
		work(Y),
	 	work(Z),
		Y\=Z,
		not(Y='������������'),
		not(Z='������������'),
		not(Y='�����������'),
		not(Z='�����������'),
		not(Y='����������'),
		not(Z='����������'),
		not(Y='�������'),
		not(Z='�������'),
		not(Y='�����'),
		not(Z='�����'),
		not(Y='������'),
		not(Z='������').

task_11(X1,Y1,Z1,X2,Y2,Z2,X3,Y3,Z3,X4,Y4,Z4):-
		X1='�������', rule(X1,Y1,Z1),
		X2='������', rule(X2,Y2,Z2),
		X3='����', rule(X3,Y3,Z3),
		X4='���', rule(X4,Y4,Z4),
		Y1\=Y2, Y2\=Y3, Y1\=Y3, Y1\=Y4, Y2\=Y4, Y3\=Y4,
		Z1\=Z2, Z2\=Z3, Z1\=Z3, Z1\=Z4, Z2\=Z4, Z3\=Z4.
task1:-
	task_11(X1,Y1,Z1,X2,Y2,Z2,X3,Y3,Z3,X4,Y4,Z4),
	write('������� - '),write(Y1),write(' � '),write(Z1),nl,
	write('������ - '),write(Y2),write(' � '),write(Z2),nl,
	write('���� - '),write(Y3),write(' � '),write(Z3),nl,
	write('��� - '),write(Y4),write(' � '),write(Z4),nl.
		
/*������� 2*/

name_2('����').
name_2('����').
name_2('����').
name_2('�����').
name_2('����').

food('�������').
food('�������').

rule_2(X,Y):-
		name_2(X),
		X='����',
		food(Y),
		not(Y='�������').
rule_2(X,Y):-
		name_2(X),
		X='����',
		food(Y),
		not(Y='�������').
rule_2(X,Y):-
		name_2(X),
		X='����',
		food(Y),
		not(Y='�������').
rule_2(X,Y):-
		name_2(X),
		X='�����',
		food(Y),
		not(Y='�������').
rule_2(X,Y):-
		name_2(X),
		X='����',
		food(Y),
		not(Y='�������').

task_21(X1,Y1):-X1='����', rule_2(X1,Y1).
task_22(X2,Y2):-X2='����', rule_2(X2,Y2).
task_23(X3,Y3):-X3='����', rule_2(X3,Y3).
task_24(X4,Y4):-X4='�����', rule_2(X4,Y4).
task_25(X5,Y5):-X5='����', rule_2(X5,Y5).

task2:-
	task_21(X1,Y1),write('���� - '),write(Y1),nl,
	task_22(X2,Y2),write('���� - '),write(Y2),nl,
	task_23(X3,Y3),write('���� - '),write(Y3),nl,
	task_24(X4,Y4),write('����� - '),write(Y4),nl,
	task_25(X5,Y5),write('���� - '),write(Y5),nl.
	
/*������� 3*/

color('�������').
color('�������').
color('�����').
color('�������').
color('������').

nat('����������').
nat('��������').
nat('��������').
nat('�����').
nat('����').

animal('����').
animal('�����').
animal('������').
animal('�����').
animal('������').

sig('PallMall').
sig('Dunhill').
sig('Marlboro').
sig('Winfield').
sig('Rothmans').

water('���').
water('����').
water('������').
water('����').
water('����').

rule(X,Y,Z,W,V):-
		nat(X),
		X='����������',
		color(Y),
		animal(Z),
		sig(W),
		water(V),
		Y\=Z, Y\=W, Y\=V, Z\=Y, Z\=W, Z\=V, W\=V,
		not(Y='�������'), not(Y='�����'), not(Y='�������'), not(Y='������'),
		not(V='���'), not(V='����'), not(V='����'), not(V='����'),
		not(W='Rothmans'), not(W='Dunhill'), not(W='Marlboro'), not(W='Winfield'),
		not(Z='������'), not(Z='����'), not(Z='�����'), not(Z='������').
		
rule(X,Y,Z,W,V):-
		nat(X),
		X='��������',
		color(Y),
		animal(Z),
		sig(W),
		water(V),
		Y\=Z, Y\=W, Y\=V, Z\=Y, Z\=W, Z\=V, W\=V,
		not(Y='�������'), not(Y='�������'), not(Y='�����'), not(Y='������'),
		not(V='����'), not(V='������'), not(V='����'), not(V='����'),
		not(W='Rothmans'), not(W='Dunhill'), not(W='Winfield'), not(W='PallMall'),
		not(Z='������'), not(Z='����'), not(Z='�����'), not(Z='�����').

rule(X,Y,Z,W,V):-
		nat(X),
		X='��������',
		color(Y),
		animal(Z),
		sig(W),
		water(V),
		Y\=Z, Y\=W, Y\=V, Z\=Y, Z\=W, Z\=V, W\=V,
		not(Y='�������'), not(Y='�������'), not(Y='�������'), not(Y='�����'),
		not(V='���'), not(V='����'), not(V='������'), not(V='����'),
		not(W='Rothmans'), not(W='Winfield'), not(W='PallMall'), not(W='Marlboro'),
		not(Z='������'), not(Z='����'), not(Z='������'), not(Z='�����').
		
		
rule(X,Y,Z,W,V):-
		nat(X),
		X='�����',
		color(Y),
		animal(Z),
		sig(W),
		water(V),
		Y\=Z, Y\=W, Y\=V, Z\=Y, Z\=W, Z\=V, W\=V,
		not(Y='�������'), not(Y='�����'), not(Y='�������'), not(Y='������'),
		not(V='���'), not(V='������'), not(V='����'), not(V='����'),
		not(W='Winfield'), not(W='PallMall'), not(W='Marlboro'), not(W='Dunhill'),
		not(Z='������'), not(Z='�����'), not(Z='������'), not(Z='�����').		
		
rule(X,Y,Z,W,V):-
		nat(X),
		X='����',
		color(Y),
		animal(Z),
		sig(W),
		water(V),
		Y\=Z, Y\=W, Y\=V, Z\=Y, Z\=W, Z\=V, W\=V,	
		not(Y='�������'), not(Y='�������'), not(Y='�������'), not(Y='������'),
		not(Z='�����'), not(Z='������'), not(Z='�����'), not(Z='����'),
		not(W='PallMall'), not(W='Marlboro'), not(W='Dunhill'),	not(W='Rothmans'),
		not(V='���'), not(V='������'), not(V='����'), not(V='����').
		
task_31(X1,Y1,Z1,W1,V1):-X1='����������', rule(X1,Y1,Z1,W1,V1).	
task_32(X2,Y2,Z2,W2,V2):-X2='��������', rule(X2,Y2,Z2,W2,V2).
task_33(X3,Y3,Z3,W3,V3):-X3='��������', rule(X3,Y3,Z3,W3,V3).	
task_34(X4,Y4,Z4,W4,V4):-X4='�����', rule(X4,Y4,Z4,W4,V4).
task_35(X5,Y5,Z5,W5,V5):-X5='����', rule(X5,Y5,Z5,W5,V5).

task3:-
	task_31(X1,Y1,Z1,W1,V1),write('����������'),nl, write('���: '),write(Y1),nl,write('��������: '),write(Z1),nl,write('��������: '),write(W1),nl,write('�������: '),write(V1),nl,nl,
	task_32(X2,Y2,Z2,W2,V2),write('��������'),nl, write('���: '),write(Y2),nl,write('��������: '),write(Z2),nl,write('��������: '),write(W2),nl,write('�������: '),write(V2),nl,nl,
	task_33(X3,Y3,Z3,W3,V3),write('��������'),nl, write('���: '),write(Y3),nl,write('��������: '),write(Z3),nl,write('��������: '),write(W3),nl,write('�������: '),write(V3),nl,nl,
	task_34(X4,Y4,Z4,W4,V4),write('�����'),nl, write('���: '),write(Y4),nl,write('��������: '),write(Z4),nl,write('��������: '),write(W4),nl,write('�������: '),write(V4),nl,nl,
	task_35(X5,Y5,Z5,W5,V5),write('����'),nl, write('���: '),write(Y5),nl,write('��������: '),write(Z5),nl,write('��������: '),write(W5),nl,write('�������: '),write(V5),nl,nl.	
