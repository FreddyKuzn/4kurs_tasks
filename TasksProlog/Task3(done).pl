﻿/* Значение n-го члена ряда Фибоначи */
fib(0,0).
fib(1,1).
fib(N,X):-
	N1 is N-1,
	N2 is N-2,
	fib(N1,X1),
	fib(N2,X2),
	X is X1+X2.

/* Произведение двух целых положительных чисел */
pr(M,1,M):-!.
pr(1,N,N):-!.
pr(M,N,R):- N1 is N-1,
	M1 is M-1,
	pr(N1,M1,R1),
	R is R1+N+M-1.

/* Сумма ряда целых четных n чисел */
sum(0,0) :- !.
sum(1,2) :- !.
sum(N,S) :- N1 is N - 1,
	 sum(N1,S1),
	 S is S1 + N*2.

/* Ввод целых положительных чисел и их суммирование до порогового значения */

checkminus(P,N,X):-
	N < -1, nl, write("It's impossible"),porog(P,X).

checkplus(P,N,X):-
	N > -1,
	Xs is X + N,
	callback(P,Xs).      

porog(P,X):-
	nl,
	write("Введите число: "),
	read(N),
	nl,
	(checkminus(P,N,X) ; checkplus(P,N,X)).

callback(P,X):-
	X<P,
	write(X),
	porog(P,X).






