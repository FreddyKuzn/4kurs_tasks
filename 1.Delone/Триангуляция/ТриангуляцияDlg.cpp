
// ������������Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "������������.h"
#include "������������Dlg.h"
#include "afxdialogex.h"
#include <time.h>
#include "math.h"
#include <ctime>
#include <string>
#include <cmath>

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// C������������Dlg dialog

#define KOORD_Signal1(x,y) (xp1*((x)-xmin1)),(yp1*((y)-ymax1))   // ��� ��������� ���������
#define KOORD_Signal1Ell(x,y) (xp1*((x)-xmin1)+3),(yp1*((y)-ymax1)+3),(xp1*((x)-xmin1)-3),(yp1*((y)-ymax1)-3)   // ��� ��������� ���������
#define RandPlus ((-1.+ 2.* rand()/RAND_MAX)*xmax1*0.0005)
C������������Dlg::C������������Dlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(C������������Dlg::IDD, pParent)
	, X1koor(130)
	, Y1koor(150)
	, X2koor(150)
	, Y2koor(120)
	, Phikoor(0.8)
	, size_isolines(25)
	, koef(30)
	, FigureDotsCount(30)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void C������������Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, X1koor);
	DDX_Text(pDX, IDC_EDIT2, Y1koor);
	DDX_Text(pDX, IDC_EDIT3, X2koor);
	DDX_Text(pDX, IDC_EDIT4, Y2koor);
	DDX_Text(pDX, IDC_EDIT5, Phikoor);
	DDX_Text(pDX, IDC_EDIT6, size_isolines);
	DDX_Text(pDX, IDC_EDIT7, koef);
	DDX_Text(pDX, IDC_EDIT8, FigureDotsCount);
}

BEGIN_MESSAGE_MAP(C������������Dlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &C������������Dlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &C������������Dlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &C������������Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &C������������Dlg::OnBnClickedButton2)
END_MESSAGE_MAP()


// C������������Dlg message handlers

BOOL C������������Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	// ����������� ������� ��� ��������� Signal
	PicWndSignal1 = GetDlgItem(IDC_Signal1);
	PicDcSignal1 = PicWndSignal1->GetDC();
	PicWndSignal1->GetClientRect(&PicSignal1);

	setka_pen.CreatePen(  //��� �����
		//PS_SOLID,    //�������� �����
		PS_DOT,     //����������
		0.2,      //������� 1 �������
		RGB(205, 155, 155));  //���� �����

	osi_pen.CreatePen(   //������������ ���
		PS_SOLID,    //�������� �����
		3,      //������� 3 �������
		RGB(138, 138, 138));   //���� ������

	graf_pen.CreatePen(   //������
		PS_SOLID,    //�������� �����
		3,      //������� 2 �������
		RGB(0, 0, 0));   //���� ������

	graf_pen1.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(205, 275, 205));   //���� �������

	graf_penred.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(255, 0, 0));   //���� �������

	graf_pengreen.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(0, 150, 0));   //���� �������

	graf_penblue.CreatePen(   //������
		PS_SOLID,    //�������� �����
		1,      //������� 2 �������
		RGB(0, 0, 0));   //���� �������
	xmin1 = 0; xmax1 = 300;
	ymin1 = 0; ymax1 = 300;
	Qmax = 1; Qmin = -1;
	Treug.clear();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void C������������Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		xp1 = ((double)(PicSignal1.Width()) / (xmax1 - xmin1));
		yp1 = -((double)(PicSignal1.Height()) / (ymax1 - ymin1));

		PicDcSignal1->FillSolidRect(&PicSignal1, RGB(255, 255, 255)); //����������� �� ������

		if (Treug.size() != NULL)
		{
			PicDcSignal1->SelectObject(&graf_penred);

			for (int i = 0; i < Treug.size(); i++)
			{
				PicDcSignal1->MoveTo(KOORD_Signal1(Treug[i].T1.x1, Treug[i].T1.y1));
				PicDcSignal1->LineTo(KOORD_Signal1(Treug[i].T2.x1, Treug[i].T2.y1));
				PicDcSignal1->LineTo(KOORD_Signal1(Treug[i].T3.x1, Treug[i].T3.y1));
				PicDcSignal1->LineTo(KOORD_Signal1(Treug[i].T1.x1, Treug[i].T1.y1));
			}
		}

		if (UnicFullDots.size() != NULL)
		{
			PicDcSignal1->SelectObject(&graf_pengreen);

			for (int i = 0; i < UnicFullDots.size(); i++)
			{
				if (UnicFullDots[i].lock) PicDcSignal1->Ellipse(KOORD_Signal1Ell(UnicFullDots[i].x1, UnicFullDots[i].y1));
			}
		}
		if (c.size() != NULL)
		{
			//for (int i = 0; i < DotC.size(); i++)
			//{
			//	int color = (int)(((DotC[i].Q + 1) / 2) * 510);
			//	int color_blue;
			//	int color_red;
			//	if (color > 255)
			//	{
			//		color_blue =0;
			//		color_red =color-255;
			//	}
			//	else
			//	{
			//		color_blue = 255 - color;
			//		color_red = 0;
			//	}
			//	CPen pen_isoline;  pen_isoline.CreatePen(   //������
			//		PS_SOLID,    //�������� �����
			//		2,      //������� 2 �������
			//		RGB(color_red, 0, color_blue));   //���� �������
			//	PicDcSignal1->SelectObject(&pen_isoline);
			//	PicDcSignal1->Ellipse(KOORD_Signal1Ell(DotC[i].x1, DotC[i].y1));
			//}
			for (int i = 0; i < isolines.size(); i++)
			{
				PicDcSignal1->SelectObject(&graf_penblue);
				for (int j = 0; j < isolines[i].line.size(); j++)
				{
					PicDcSignal1->MoveTo(KOORD_Signal1(isolines[i].line[j].first.x1, isolines[i].line[j].first.y1));
					PicDcSignal1->LineTo(KOORD_Signal1(isolines[i].line[j].second.x1, isolines[i].line[j].second.y1));
				}
			}
		}
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR C������������Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void C������������Dlg::OnBnClickedOk()
{

	UpdateData(TRUE);

	c.clear(); DotC.clear(); UnicFullDots.clear(); isolines.clear(); //New Task
	Dot dotbuffer;
	//===================================================================
	//��������� 4 ����� ��������������.
	//===================================================================
	GiperDot.resize(0);
	dotbuffer.x1 = 0.; dotbuffer.y1 = 0;
	GiperDot.push_back(dotbuffer);
	dotbuffer.x1 = xmax1; dotbuffer.y1 = 0;
	GiperDot.push_back(dotbuffer);
	dotbuffer.x1 = 0; dotbuffer.y1 = xmax1;
	GiperDot.push_back(dotbuffer);
	dotbuffer.x1 = xmax1; dotbuffer.y1 = xmax1;
	GiperDot.push_back(dotbuffer);
	//===================================================================
	//��������� ������ - ��������������
	//===================================================================
	double gran = 0.1;
	double gransetka = 0.15;
	double step = (1. - 2 * gran)*xmax1 / koef;
	for (int i = 0; i <= koef; i++)
	{
		dotbuffer.x1 = gran * xmax1 + RandPlus;
		dotbuffer.y1 = gran * xmax1 + i * step + RandPlus;
		GiperDot.push_back(dotbuffer);
		dotbuffer.x1 = (1 - gran)*xmax1 + RandPlus;
		GiperDot.push_back(dotbuffer);
	}
	for (int i = 1; i <= koef - 1; i++)
	{
		dotbuffer.y1 = gran * xmax1 + RandPlus;
		dotbuffer.x1 = gran * xmax1 + i * step + RandPlus;
		GiperDot.push_back(dotbuffer);
		dotbuffer.y1 = (1 - gran)*xmax1 + RandPlus;
		GiperDot.push_back(dotbuffer);
	}
	//===================================================================
	//��������� ������
	//===================================================================
	//���������� ��� ������
	Dot cen;
	double phi;
	double a;
	int rad1, rad2;

	//===================================================================
// ������ �������
	rad1 = 15; rad2 = 25;
	phi = Phikoor; //���� �������� (� ������)
	cen.x1 = X1koor; // ����� 1 �������
	cen.y1 = Y1koor; // ����� 1 �������
	vector<Dot> A1; // ������ ������ �������
	int kol_tc = FigureDotsCount; //���������� ����� �� ����
	int ten = 5;
	//======================================================

	//for (int i = 0; i <= kol_tc / 2; i++)
	//{
	//	a = (double)i / (double)kol_tc * M_PI * 2.0;
	//	dotbuffer.x1 = cos(a + phi) * rad2 + cen.x1;
	//	dotbuffer.y1 = sin(a + phi) * rad2 + cen.y1;
	//	dotbuffer.Q = Qmax;//New Task
	//	GiperDot.push_back(dotbuffer);
	//	A1.push_back(dotbuffer);
	//}
	//for (int rad = rad2; rad >= 0; rad -= ten)
	//{
	//	a = M_PI;
	//	dotbuffer.x1 = cos(a + phi) * rad + cen.x1 + RandPlus;
	//	dotbuffer.y1 = sin(a + phi) * rad + cen.y1 + RandPlus;
	//	dotbuffer.Q = Qmax;//New Task
	//	GiperDot.push_back(dotbuffer);
	//	A1.push_back(dotbuffer);
	//}
	//for (int rad = ten; rad <= rad2; rad += ten)
	//{
	//	a = 0;
	//	dotbuffer.x1 = cos(a + phi) * rad + cen.x1 + RandPlus;
	//	dotbuffer.y1 = sin(a + phi) * rad + cen.y1 + RandPlus;
	//	dotbuffer.Q = Qmax;//New Task
	//	GiperDot.push_back(dotbuffer);
	//	A1.push_back(dotbuffer);
	//}
	//=================================================================
	for (int rad = rad1; rad <= rad2; rad += ten)
	{
		for (int i = 0; i <= kol_tc / 2; i++)
		{
			a = (double)i / (double)kol_tc * M_PI * 2.0;
			dotbuffer.x1 = cos(a + phi) * rad + cen.x1 + RandPlus;
			dotbuffer.y1 = sin(a + phi) * rad + cen.y1 + RandPlus;
			dotbuffer.Q = 1;//New Task
			GiperDot.push_back(dotbuffer);
		}
	}
	for (int i = 0; i <= kol_tc / 2; i++)
	{
		a = (double)i / (double)kol_tc * M_PI * 2.0;
		dotbuffer.x1 = cos(a + phi) * rad1 + cen.x1 + RandPlus;
		dotbuffer.y1 = sin(a + phi) * rad1 + cen.y1 + RandPlus;
		dotbuffer.Q = 1;//New Task
		A1.push_back(dotbuffer);
	}
	for (int i = kol_tc / 2; i >= 0; i--)
	{
		a = (double)i / (double)kol_tc * M_PI * 2.0;
		dotbuffer.x1 = cos(a + phi) * rad2 + cen.x1 + RandPlus;
		dotbuffer.y1 = sin(a + phi) * rad2 + cen.y1 + RandPlus;
		dotbuffer.Q = 1;//New Task
		A1.push_back(dotbuffer);
	}
	//===================================================================
// ������ �������
	rad1 = 15; rad2 = 25;
	phi = Phikoor + M_PI;
	cen.x1 = X2koor;
	cen.y1 = Y2koor;
	vector<Dot> A2;
	//=================================================================

	//for (int i = 0; i <= kol_tc / 2; i++)
	//{
	//	a = (double)i / (double)kol_tc * M_PI * 2.0;
	//	dotbuffer.x1 = cos(a + phi) * rad2 + cen.x1;
	//	dotbuffer.y1 = sin(a + phi) * rad2 + cen.y1;
	//	dotbuffer.Q = Qmin;//New Task
	//	GiperDot.push_back(dotbuffer);
	//	A2.push_back(dotbuffer);
	//}
	//for (int rad = rad2; rad >= 0; rad -= ten)
	//{
	//	a = M_PI;
	//	dotbuffer.x1 = cos(a + phi) * rad + cen.x1 + RandPlus;
	//	dotbuffer.y1 = sin(a + phi) * rad + cen.y1 + RandPlus;
	//	dotbuffer.Q = Qmin;//New Task
	//	GiperDot.push_back(dotbuffer);
	//	A2.push_back(dotbuffer);
	//}
	//for (int rad = ten; rad <= rad2; rad += ten)
	//{
	//	a = 0;
	//	dotbuffer.x1 = cos(a + phi) * rad + cen.x1 + RandPlus;
	//	dotbuffer.y1 = sin(a + phi) * rad + cen.y1 + RandPlus;
	//	dotbuffer.Q = Qmin;//New Task
	//	GiperDot.push_back(dotbuffer);
	//	A2.push_back(dotbuffer);
	//}
	//===========================================================
	for (int rad = rad1; rad <= rad2; rad += ten)
	{
		for (int i = 0; i <= kol_tc / 2; i++)
		{
			a = (double)i / (double)kol_tc * M_PI * 2.0;
			dotbuffer.x1 = cos(a + phi) * rad + cen.x1 + RandPlus;
			dotbuffer.y1 = sin(a + phi) * rad + cen.y1 + RandPlus;
			dotbuffer.Q = -1;//New Task
			GiperDot.push_back(dotbuffer);
		}
	}
	for (int i = 0; i <= kol_tc / 2; i++)
	{
		a = (double)i / (double)kol_tc * M_PI * 2.0;
		dotbuffer.x1 = cos(a + phi) * rad1 + cen.x1 + RandPlus;
		dotbuffer.y1 = sin(a + phi) * rad1 + cen.y1 + RandPlus;
		dotbuffer.Q = -1;//New Task
		A2.push_back(dotbuffer);
	}
	for (int i = kol_tc / 2; i >= 0; i--)
	{
		a = (double)i / (double)kol_tc * M_PI * 2.0;
		dotbuffer.x1 = cos(a + phi) * rad2 + cen.x1 + RandPlus;
		dotbuffer.y1 = sin(a + phi) * rad2 + cen.y1 + RandPlus;
		dotbuffer.Q = -1;//New Task
		A2.push_back(dotbuffer);
	}
	//===================================================================
	//������������ ������
	generator(GiperDot, Treug, 1); //��������� ������������ ��������������
	//===================================================================
	//��������. ������
	//===================================================================

	step = (1. - 2 * gransetka)*xmax1 / koef;
	vector <Dot> BufferDot;// ����� ����� ������������
	vector <Dot> BufferDot2;//���������� ����� ����� ������������
	vector <Tringl> BufferTreug;// ����� ������������
	for (int i = 0; i <= koef; i++)
	{
		for (int j = 0; j <= koef; j++)
		{
			BufferDot.clear();
			BufferDot2.clear();
			dotbuffer.x1 = gransetka * xmax1 + j * step + RandPlus;
			dotbuffer.y1 = gransetka * xmax1 + i * step + RandPlus;
			dotbuffer.lock = 0;

			if (isInside(A1, dotbuffer, A1.size()) || isInside(A2, dotbuffer, A2.size()))
			{
				continue;
			}
			else
			{
				DotC.push_back(dotbuffer);//New Task
				for (int k = 0; k < Treug.size(); k++)
				{
					double D = RadiusOkr(Treug[k].C, dotbuffer);
					if (D <= Treug[k].RadiusO) // ����� ������ �����!
					{
						BufferDot.push_back(Treug[k].T1);
						BufferDot.push_back(Treug[k].T2);
						BufferDot.push_back(Treug[k].T3);
						Treug.erase(Treug.begin() + k);
						k--;
					}

				}
				bool rrr;
				for (int k = 0; k < BufferDot.size(); k++)
				{
					rrr = false;
					for (int m = 0; m < BufferDot2.size(); m++)
					{
						if (BufferDot[k].x1 == BufferDot2[m].x1 && BufferDot[k].y1 == BufferDot2[m].y1)
						{
							rrr = true;
							break;
						}
					}
					if (!rrr)BufferDot2.push_back(BufferDot[k]);
				}
				BufferDot2.push_back(dotbuffer);
				generator(BufferDot2, BufferTreug, 0);
				for (int k = 0; k < BufferTreug.size(); k++)
				{
					if ((dotbuffer.x1 == BufferTreug[k].T1.x1 && dotbuffer.y1 == BufferTreug[k].T1.y1) || (dotbuffer.x1 == BufferTreug[k].T2.x1 && dotbuffer.y1 == BufferTreug[k].T2.y1) || (dotbuffer.x1 == BufferTreug[k].T3.x1 && dotbuffer.y1 == BufferTreug[k].T3.y1))
					{
						if (!(isInside(A1, BufferTreug[k].C, A1.size()) || isInside(A2, BufferTreug[k].C, A2.size())))
							Treug.push_back(BufferTreug[k]);
					}
				}
			}
		}
	}

	for (int i = 0; i < Treug.size(); i++)
	{
		if (Treug[i].T1.lock && Treug[i].T2.lock && Treug[i].T3.lock)
		{
			Treug.erase(Treug.begin() + i);
			i--;
		}
	}

	UpdateData(FALSE);
	Invalidate();

}


void C������������Dlg::KoorCenter(Dot T1, Dot T2, Dot T3, Dot &TC)
{
	TC.x1 = 0.5*((T2.x1* T2.x1 - T1.x1* T1.x1 + T2.y1* T2.y1 - T1.y1* T1.y1)*(T3.y1 - T1.y1) - (T3.x1* T3.x1 - T1.x1* T1.x1 + T3.y1* T3.y1 - T1.y1* T1.y1)*(T2.y1 - T1.y1));
	TC.x1 /= ((T2.x1 - T1.x1)*(T3.y1 - T1.y1) - (T3.x1 - T1.x1)*(T2.y1 - T1.y1));

	TC.y1 = 0.5*((T3.x1* T3.x1 - T1.x1* T1.x1 + T3.y1* T3.y1 - T1.y1* T1.y1)*(T2.x1 - T1.x1) - (T2.x1* T2.x1 - T1.x1* T1.x1 + T2.y1* T2.y1 - T1.y1* T1.y1)*(T3.x1 - T1.x1));
	TC.y1 /= ((T2.x1 - T1.x1)*(T3.y1 - T1.y1) - (T3.x1 - T1.x1)*(T2.y1 - T1.y1));
}
double C������������Dlg::RadiusOkr(Dot TC, Dot T)
{
	return sqrt((TC.x1 - T.x1)*(TC.x1 - T.x1) + (TC.y1 - T.y1)*(TC.y1 - T.y1));
}


void C������������Dlg::OnBnClickedCancel()
{
	// TODO: �������� ���� ��� ����������� �����������
	CDialogEx::OnCancel();
}

void C������������Dlg::generator(vector <Dot> Koor, vector<Tringl> &Treg, bool lock)
{
	bool OnOff = 0;
	Treg.clear();
	for (int i = 0; i < Koor.size(); i++)
	{
		for (int j = i + 1; j < Koor.size(); j++)
		{
			for (int k = j + 1; k < Koor.size(); k++)
			{
				OnOff = 0;
				Dot Centr;
				KoorCenter(Koor[i], Koor[j], Koor[k], Centr);
				double Radius = RadiusOkr(Centr, Koor[i]);
				for (int p = 0; p < Koor.size(); p++)
				{
					if (p != i && p != j && p != k)
					{
						double D = RadiusOkr(Centr, Koor[p]);
						if (D <= Radius)
						{
							OnOff = 1;
							break;
						}
						else continue;
					}
					else continue;
				}
				if (!OnOff)
				{
					Tringl bufer;
					//	bufer.lock = lock;
					bufer.C = Centr;
					bufer.RadiusO = Radius;
					bufer.T1 = Koor[i];
					bufer.T2 = Koor[j];
					bufer.T3 = Koor[k];
					Treg.push_back(bufer);
				}
			}
		}
	}
}
//========================================================================
//========================================================================
//========================================================================
//========================================================================
//NEW TASK
//========================================================================
//========================================================================
//========================================================================

void C������������Dlg::OnBnClickedButton1()
{
	//������� ���������� ����� �� ������������
	FullDots.resize(0);
	UnicFullDots.resize(0);
	for (int i = 0; i < Treug.size(); i++)
	{
		FullDots.push_back(Treug[i].T1); // ���������� ������� ���� �������������	
		FullDots.push_back(Treug[i].T2);
		FullDots.push_back(Treug[i].T3);
	}
	FindUnicDots(FullDots, UnicFullDots); // ���������� ��������������� �����
	for (int i = 0; i < UnicFullDots.size(); i++) // �������� ������ ����� ���� ������
	{
		UnicFullDots[i].index = i;
	}
	//======================================
	//����������� ���������� ������������� ����������� �������
	for (int i = 0; i < Treug.size(); i++)
	{
		for (int j = 0; j < UnicFullDots.size(); j++) // ����������� ������� ���������� �����
		{
			if (EquallyDots(Treug[i].T1, UnicFullDots[j]))
			{
				Treug[i].T1.index = UnicFullDots[j].index;
				continue;
			}
			if (EquallyDots(Treug[i].T2, UnicFullDots[j]))
			{
				Treug[i].T2.index = UnicFullDots[j].index;
				continue;
			}
			if (EquallyDots(Treug[i].T3, UnicFullDots[j]))
			{
				Treug[i].T3.index = UnicFullDots[j].index;
				continue;
			}
		}
	}
	//======================================
	for (int i = 0; i < Treug.size(); i++)
	{
		bool gbI;
		gbI = AddNewNeighbors(UnicFullDots[Treug[i].T1.index], Treug[i].T2); // ���������� ������� ��������� ������� ( + ���������� �����)
		gbI = AddNewNeighbors(UnicFullDots[Treug[i].T1.index], Treug[i].T3);

		gbI = AddNewNeighbors(UnicFullDots[Treug[i].T2.index], Treug[i].T1);
		gbI = AddNewNeighbors(UnicFullDots[Treug[i].T2.index], Treug[i].T3);

		gbI = AddNewNeighbors(UnicFullDots[Treug[i].T3.index], Treug[i].T1);
		gbI = AddNewNeighbors(UnicFullDots[Treug[i].T3.index], Treug[i].T2);
	}
	//=======================================

	Invalidate();
}
void C������������Dlg::OnBnClickedButton2()
{
	UpdateData(1);
	isolines.clear();
	Bj.clear();
	A.clear();
	c.clear(); //������ �����������
	Galerkin(Bj, A, Treug);
	kazf(A, Bj, c, Bj.size(), Bj.size());// ������� ������������� ������� (������ �) ������� �������
	for (int i = 0; i < c.size(); i++)
		DotC[i].Q = c[i];
	//================================================
	//����������� �������� �������� � ������ �� ������� "c" � ������ Treug
	for (int i = 0; i < Treug.size(); i++)
	{
		if (!Treug[i].T1.lock)
		{
			for (int j = 0; j < DotC.size(); j++)
			{
				if (EquallyDots(Treug[i].T1, DotC[j]))
					Treug[i].T1.Q = DotC[j].Q;
			}
		}
		if (!Treug[i].T2.lock)
		{
			for (int j = 0; j < DotC.size(); j++)
			{
				if (EquallyDots(Treug[i].T2, DotC[j]))
					Treug[i].T2.Q = DotC[j].Q;
			}
		}
		if (!Treug[i].T3.lock)
		{
			for (int j = 0; j < DotC.size(); j++)
			{
				if (EquallyDots(Treug[i].T3, DotC[j]))
					Treug[i].T3.Q = DotC[j].Q;
			}
		}
	}
	//================================================
	//���������� ��������
	isolines.resize(size_isolines);
	Qmax = 1; Qmin = -1;
	Qmax = Qmax / 3;//���������� ��������� ���������� ��������
	Qmin = Qmin / 3;
	double isolinesStep = (Qmax - Qmin) / size_isolines;
	for (int i = 0; i < size_isolines; i++)
	{
		isolines[i].Q = Qmin + i * isolinesStep;
	}
	for (int i = 0; i < Treug.size(); i++)
	{
		//�������� �� ���������� ����� ������������ � ���������� �������
		if (!Treug[i].T1.lock && !Treug[i].T2.lock && !Treug[i].T3.lock)
		{
			vector<Dot> QQQ;
			QQQ.push_back(Treug[i].T1);
			QQQ.push_back(Treug[i].T2);
			QQQ.push_back(Treug[i].T3);
			BubleSortQ(QQQ);

			for (int j = 0; j < isolines.size(); j++)
			{
				if (isolines[j].Q > QQQ[0].Q && isolines[j].Q < QQQ[1].Q)
				{
					pair<Dot, Dot>XYC;
					XYC.first.x1 = QQQ[1].x1 - ((QQQ[1].Q - isolines[j].Q) / (QQQ[1].Q - QQQ[0].Q))*(QQQ[1].x1 - QQQ[0].x1); //��������� ������������� �.2 ���.38 (���������� �1)
					XYC.first.y1 = QQQ[1].y1 - ((QQQ[1].Q - isolines[j].Q) / (QQQ[1].Q - QQQ[0].Q))*(QQQ[1].y1 - QQQ[0].y1);
					XYC.second.x1 = QQQ[2].x1 - ((QQQ[2].Q - isolines[j].Q) / (QQQ[2].Q - QQQ[0].Q))*(QQQ[2].x1 - QQQ[0].x1);
					XYC.second.y1 = QQQ[2].y1 - ((QQQ[2].Q - isolines[j].Q) / (QQQ[2].Q - QQQ[0].Q))*(QQQ[2].y1 - QQQ[0].y1);
					isolines[j].line.push_back(XYC);
					//continue;
				}
				if (isolines[j].Q > QQQ[1].Q && isolines[j].Q < QQQ[2].Q)
				{
					pair<Dot, Dot>XYC;
					XYC.first.x1 = QQQ[2].x1 - ((QQQ[2].Q - isolines[j].Q) / (QQQ[2].Q - QQQ[0].Q))*(QQQ[2].x1 - QQQ[0].x1); //��������� ������������� �.2 ���.38 (���������� �1)
					XYC.first.y1 = QQQ[2].y1 - ((QQQ[2].Q - isolines[j].Q) / (QQQ[2].Q - QQQ[0].Q))*(QQQ[2].y1 - QQQ[0].y1);
					XYC.second.x1 = QQQ[2].x1 - ((QQQ[2].Q - isolines[j].Q) / (QQQ[2].Q - QQQ[1].Q))*(QQQ[2].x1 - QQQ[1].x1);
					XYC.second.y1 = QQQ[2].y1 - ((QQQ[2].Q - isolines[j].Q) / (QQQ[2].Q - QQQ[1].Q))*(QQQ[2].y1 - QQQ[1].y1);
					isolines[j].line.push_back(XYC);
				}
			}
		}
	}


	Invalidate();
}

void C������������Dlg::FindUnicDots(vector<Dot> mas1, vector<Dot> &mas2)
{
	mas2.resize(0);
	bool unictrue;//������������� ����������
	for (int k = 0; k < mas1.size(); k++)
	{
		unictrue = false;
		for (int m = 0; m < mas2.size(); m++)
		{
			if (mas1[k].x1 == mas2[m].x1 && mas1[k].y1 == mas2[m].y1)
			{
				unictrue = true;
				break;
			}
		}
		if (!unictrue)mas2.push_back(mas1[k]);
	}
}
bool C������������Dlg::EquallyDots(Dot dot1, Dot dot2)
{
	return (dot1.x1 == dot2.x1 && dot1.y1 == dot2.y1);
}


bool C������������Dlg::AddNewNeighbors(Dot &dot1, Dot dot2)
{
	bool unictrue = true;//������������� ����������
	for (int i = 0; i < dot1.neighbors.size(); i++)
	{
		if (dot1.neighbors[i] == dot2.index)
		{
			unictrue = false;
			break;
		}
	}
	if (unictrue) dot1.neighbors.push_back(dot2.index);
	return unictrue;
}
void C������������Dlg::Coff_A(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double &AI, double &BI)
{
	AI = y1 * (z2 - z3) + y2 * (z3 - z1) + y3 * (z1 - z2);
	BI = z1 * (x2 - x3) + z2 * (x3 - x1) + z3 * (x1 - x2);
}

void C������������Dlg::Coff_B(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double &AJ, double &BJ)
{
	AJ = y1 * (z2 - z3) + y2 * (z3 - z1) + y3 * (z1 - z2);
	BJ = z1 * (x2 - x3) + z2 * (x3 - x1) + z3 * (x1 - x2);
}

double C������������Dlg::Coff_A_B(int i, int j, int h)
{
	double Ai = 0;
	double Bi = 0;
	double Aj = 0;
	double Bj = 0;
	double S = 0;
	UnicFullDots[i].z1 = 1.0;
	Coff_A(UnicFullDots[Treug[h].T1.index].x1, UnicFullDots[Treug[h].T1.index].y1, UnicFullDots[Treug[h].T1.index].z1, UnicFullDots[Treug[h].T2.index].x1, UnicFullDots[Treug[h].T2.index].y1, UnicFullDots[Treug[h].T2.index].z1, UnicFullDots[Treug[h].T3.index].x1,
		UnicFullDots[Treug[h].T3.index].y1, UnicFullDots[Treug[h].T3.index].z1, Ai, Bi);
	UnicFullDots[i].z1 = 0;
	UnicFullDots[j].z1 = 1;
	Coff_B(UnicFullDots[Treug[h].T1.index].x1, UnicFullDots[Treug[h].T1.index].y1, UnicFullDots[Treug[h].T1.index].z1, UnicFullDots[Treug[h].T2.index].x1, UnicFullDots[Treug[h].T2.index].y1, UnicFullDots[Treug[h].T2.index].z1, UnicFullDots[Treug[h].T3.index].x1,
		UnicFullDots[Treug[h].T3.index].y1, UnicFullDots[Treug[h].T3.index].z1, Aj, Bj);
	UnicFullDots[j].z1 = 0;
	S = 1.0;
	return S * S + Ai * Aj + S * S*Bi*Bj;
}

void C������������Dlg::Galerkin(vector<double> &BJ, vector<double> &A, vector<Tringl>Treugol)
{
	int countA = 0;
	int countB = 0;
	//index.clear();
	for (int i = 0; i < UnicFullDots.size(); i++)
	{
		UnicFullDots[i].z1 = 0;
	}
	for (int i = 0; i < UnicFullDots.size(); i++) {

		if (UnicFullDots[i].lock == false) //���� ����� �� �����
		{
			for (int j = 0; j < UnicFullDots.size(); j++)
			{
				if (UnicFullDots[j].lock == false)//���� ����� �� �����
				{
					countA++;
					A.resize(countA);
					A[countA - 1] = 0;
					if (i == j)
					{
						for (int h = 0; h < Treug.size(); h++)
						{
							if (Treug[h].T1.index == i || Treug[h].T2.index == i || Treug[h].T3.index == i)
							{
								A[countA - 1] += Coff_A_B(i, j, h);
							}
						}
					}
					else
					{
						int tmp = 0;
						for (int p = 0; p < UnicFullDots[i].neighbors.size(); p++)
						{

							if (UnicFullDots[i].neighbors[p] == j)
							{
								tmp++;

								for (int h = 0; h < Treug.size(); h++)
								{

									if ((Treug[h].T1.index == i || Treug[h].T2.index == i || Treug[h].T3.index == i)
										&& (Treug[h].T1.index == j || Treug[h].T2.index == j || Treug[h].T3.index == j))
									{
										A[countA - 1] += Coff_A_B(i, j, h);
									}
								}
							}
						}
						if (tmp == 0)
						{
							A[countA - 1] = 0;
						}
					}

				}
			}
			countB++;
			BJ.resize(countB);
			//index.push_back(i);
			BJ[countB - 1] = 0;
			int tmp = 0;
			for (int p = 0; p < UnicFullDots[i].neighbors.size(); p++)
			{
				if (UnicFullDots[UnicFullDots[i].neighbors[p]].lock == true)
				{
					tmp++;
					int j = UnicFullDots[i].neighbors[p];
					for (int h = 0; h < Treug.size(); h++) {

						if ((Treug[h].T1.index == i || Treug[h].T2.index == i || Treug[h].T3.index == i) && (Treug[h].T1.index == j || Treug[h].T2.index == j || Treug[h].T3.index == j)) {

							BJ[countB - 1] += UnicFullDots[UnicFullDots[i].neighbors[p]].Q*Coff_A_B(i, j, h);
						}
					}
				}
			}
			if (tmp == 0)
			{
				BJ[countB - 1] = 0;
			}
		}
	}
}


void C������������Dlg::kazf(vector<double> a, vector<double> b, vector<double>  &x, int nn, int ny)
{
	x.clear();
	x.resize(nn);
	// nn - ���������� �����������;  ny - ���������� ���������
	double eps = 1.e-6f;
	//double s;
	int i, j, k;
	double s1, s2, fa1, t;
	double *x1;

	x1 = new double[nn];

	x[0] = 0.5f;
	for (i = 1; i < nn; i++)  x[i] = 0.f;

	s1 = s2 = 1.f;
	while (s1 > eps*s2)
	{
		for (i = 0; i < nn; i++) x1[i] = x[i];
		for (i = 0; i < ny; i++)
		{
			s1 = 0.0;
			s2 = 0.0;
			for (j = 0; j < nn; j++)
			{
				fa1 = a[i*nn + j];
				s1 += fa1 * x[j];
				s2 += fa1 * fa1;
			}
			t = (b[i] - s1) / s2;
			for (k = 0; k < nn; k++)    x[k] += a[i*nn + k] * t;
		}

		s1 = 0.0;
		s2 = 0.0;
		for (i = 0; i < nn; i++)
		{
			s1 += (x[i] - x1[i]) * (x[i] - x1[i]);
			s2 += x[i] * x[i];
		}
		s1 = (double)sqrt(s1);
		s2 = (double)sqrt(s2);
	}
	delete[] x1;
}

void C������������Dlg::BubleSortQ(vector<Dot>&A)
{
	Dot temp; // ��������� ���������� ��� ������ ��������� �������
	// ���������� ������� ���������
	for (int i = 0; i < A.size() - 1; i++)
	{
		for (int j = 0; j < A.size() - i - 1; j++)
		{
			if (A[j].Q > A[j + 1].Q)
			{
				// ������ �������� �������
				temp = A[j];
				A[j] = A[j + 1];
				A[j + 1] = temp;
			}
		}
	}
}