#include <vector>
#include "afxwin.h"
// ������������Dlg.h : header file

#define M_PI 3.1415926535897932384626433832795
using namespace std;
#pragma once



// C������������Dlg dialog
class C������������Dlg : public CDialogEx
{
// Construction
public:
	C������������Dlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MY_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	struct Dot //�����
	{
		double x1, y1,z1=0; //����������
		bool lock = 1; // 1- ����� ��������������, 0 - ����� �����
		double Q = 0; //����� � ����� �� ��������� ��������
		int index; //������ �����
		vector <int> neighbors;
	};
	struct Tringl //�����������
	{
		Dot T1, T2, T3;//�����
		double RadiusO; //������ ��������� ����������
		Dot C;// ����� ��������� ���������� 
	};


	double xp1, yp1,        // ����������� ���������
		xmin1, xmax1,	    // �������������� � ����������� �������� ������ ������
		ymin1, ymax1;       // �������������� � ����������� �������� ������ ������

	// ������� ������ CWnd
	CWnd * PicWndSignal1;

	// ������� ������ CDC
	CDC * PicDcSignal1;

	// ������� ������ CRect
	CRect PicSignal1;

	//�����
	CPen osi_pen;
	CPen setka_pen;
	CPen graf_pen;
	CPen graf_pen1;
	CPen graf_penred;
	CPen graf_pengreen;
	CPen graf_penblue;

	int koef;

	
	void KoorCenter(Dot T1, Dot T2, Dot T3, Dot &TC); 
	double RadiusOkr(Dot TC, Dot T);
	vector <Dot> GiperDot;
	vector <Tringl> Treug; //������ �������������(����)
	void generator(vector <Dot> Koor, vector<Tringl> &Treg, bool lock);//������� ���������� ������������ ���������

	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButton1();
	//=====================================================================
	//������� �������� �������������� ����� ��������������
	double getAzimuth(const Dot o, const Dot p) {
		return atan2(p.y1 - o.y1, p.x1 - o.x1);
	}

	double getAngle(const Dot o, const Dot a, const Dot b) {
		double result = getAzimuth(o, b) - getAzimuth(o, a);
		if (result > M_PI) result -= 2 * M_PI;
		if (result < -M_PI) result += 2 * M_PI;
		return result;
	}

	bool isInside(vector<Dot> P, const Dot p, unsigned n) {
		double sum = getAngle(p, P[n - 1], P[0]);
		while (--n) {
			sum += getAngle(p, P[n - 1], P[n]);
		}
		return fabs(sum) > 1e-10;
	}
	//========================================================================
	//���������� ��� ���������� �����
	double X1koor;
	double Y1koor;
	double X2koor;
	double Y2koor;
	double Phikoor;
	//========================================================================
	//========================================================================
	//========================================================================
	//========================================================================
	//NEW TASK
	//========================================================================
	//========================================================================
	//========================================================================
	struct Isoline
	{
		vector<pair<Dot,Dot>> line; //����� �������� �� ������
		double Q; //�������� Q �� ��������
	};
	vector <Dot> FullDots; //������ ���� ����� ������������
	vector <Dot> UnicFullDots;
	void FindUnicDots(vector<Dot> mas1, vector<Dot> &mas2);//������� ������ ���������� �����
	bool EquallyDots(Dot dot1, Dot dot2);//������� ���������� �����
	bool AddNewNeighbors(Dot &dot1, Dot dot2); // ���������� ������ ����� ����� � ������
	void Coff_A(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double &AI, double &BI);
	void Coff_B(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double &AJ, double &BJ);
	double Coff_A_B(int i, int j, int h);
	void Galerkin(vector<double> &BJ, vector<double> &A, vector<Tringl>Treugol);
	void kazf(vector<double> a, vector<double> b, vector<double>  &x, int nn, int ny);
	void BubleSortQ(vector<Dot> &A);
	vector<double>A;
	vector<double>Bj;
	vector<double>c;//������������� Q (������������ �� kazf)
	vector<Dot> DotC;//������ ����� � ��������� �������������� Q
	double Qmax, Qmin;
	vector<Isoline> isolines;//��������
	afx_msg void OnBnClickedButton2();
	int size_isolines;
	int FigureDotsCount;
};
